$(document).ready(function(){
    $('[data-toggle="popover"]').popover({
    });
});

function wrong_fields_in_login() {
    $("#wrong_authorization").fadeTo(2000, 500).slideUp(500, function () {
        $("#wrong_authorization").slideUp(500);
    });
}
function valid_authorization() {
    let a = document.forms["login_form"]["login"].value;
    let b = document.forms["login_form"]["password"].value;
    if (a == "" || a == null || b == "" || b == null) {
        $("#empty_fields").fadeTo(2000, 500).slideUp(500, function () {
            $("#empty_fields").slideUp(500);
        });
        return false;
    }
    return true;
}
function valid_player_change() {
    let a = document.forms["player_form"]["jokey"].value;
    let b = document.forms["player_form"]["horse"].value;
    if (a == "" || a == null || b == "" || b == null) {
        $("#empty_fields").fadeTo(2000, 500).slideUp(500, function () {
            $("#empty_fields").slideUp(500);
        });
        return false;
    }
    return true;
}
function valid_track_change() {
    let a = document.forms["track_form"]["name"].value;
    let b = document.forms["track_form"]["type"].value;
    let c = document.forms["track_form"]["city"].value;
    if (a == "" || a == null || b == "" || b == null || c == "" || c == null) {
        $("#empty_fields").fadeTo(2000, 500).slideUp(500, function () {
            $("#empty_fields").slideUp(500);
        });
        return false;
    }
    return true;
}

function valid_race_change() {
    let a = document.forms["race_form"]["name"].value;
    let b = document.forms["race_form"]["date"].value;
    let c = document.forms["race_form"]["time"].value;
    let d = document.forms["race_form"]["track"].value;
    if (a == "" || a == null || b == "" || b == null || c == "" || c == null || d == "" || d == null) {
        $("#empty_fields").fadeTo(2000, 500).slideUp(500, function () {
            $("#empty_fields").slideUp(500);
        });
        return false;
    }
    return true;
}
function valid_registration() {
    let a = document.forms["registration_form"]["login"].value;
    let b = document.forms["registration_form"]["password"].value;
    let c = document.forms["registration_form"]["fname"].value;
    let d = document.forms["registration_form"]["sname"].value;
    let e = document.forms["registration_form"]["passport"].value;
    let f = document.forms["registration_form"]["balance"].value;
    if (a == "" || a == null || b == "" || b == null || c == null || c == ""
        || d == "" || d == null || e == "" || e == null || f == "" || f == null) {
        $("#empty_fields").fadeTo(2000, 500).slideUp(500, function () {
            $("#empty_fields").slideUp(500);
        });
        return false;
    }
    return true;
}
function valid_cabinet() {
    let c = document.forms["registration_form"]["fname"].value;
    let d = document.forms["registration_form"]["sname"].value;
    let e = document.forms["registration_form"]["passport"].value;
    let f = document.forms["registration_form"]["balance"].value;
    if (c == null || c == "" || d == "" || d == null || e == "" || e == null || f == "" || f == null) {
        $("#empty_fields").fadeTo(2000, 500).slideUp(500, function () {
            $("#empty_fields").slideUp(500);
        });
        return false;
    }
    return true;
}
function valid_user_change() {
    let a = document.forms["registration_form"]["login"].value;
    let b = document.forms["registration_form"]["password"].value;
    if (a == "" || a == null || b == "" || b == null) {
        $("#empty_fields").fadeTo(2000, 500).slideUp(500, function () {
            $("#empty_fields").slideUp(500);
        });
        return false;
    }
    return true;
}

function disable_place(selectElement) {
    let arraySelects = document.getElementsByClassName('mySelect');
    let selectedOption = selectElement.selectedIndex;
    let button = document.getElementById('close');
    let b = true;
    for (let i = 0; i < arraySelects.length; i++) {
        if (arraySelects[i] == selectElement)
            continue;
        arraySelects[i].options[selectedOption].disabled = true;
    }
    for (let i = 0; i < arraySelects.length; i++) {
        if (arraySelects[i].selectedIndex == '0') {
            b = false;
        }
    }
    if (b) {
        button.disabled = false;
    }
}

function show_message(name) {
    $(name).fadeTo(2000, 500).slideUp(500, function () {
        $(name).slideUp(500);
    });
}
function confirm_delete() {
    let r = confirm("Are you shure?");
    return r == true;
}

function valid_rate() {
    let a = document.forms["rate_form"]["maxcount"].value;
    let b = document.forms["rate_form"]["minsumm"].value;
    let c = document.forms["rate_form"]["maxsumm"].value;
    let d = document.forms["rate_form"]["coeff"].value;
    if (a == null || a == "" || b == "" || b == null || c == "" || c == null || d == "" || d == null) {
        $("#empty_fields").fadeTo(2000, 500).slideUp(500, function () {
            $("#empty_fields").slideUp(500);
        });
        return false;
    }
    return true;
}