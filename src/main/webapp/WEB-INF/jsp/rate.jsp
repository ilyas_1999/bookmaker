<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<my:body>
    <div class="box" style="width: 40%;  height: 70%; left: 50%; top:50%; margin: -15% auto 0 -18%;">
        <div class="box-stitches-top"></div>
        <h2>Minimum rate: ${rate.getMinsumm()}, maximum: ${rate.getMaxsumm()}</h2>
        <h2>Your possible reward is ${possible} multiple on your rate!</h2>
        <h2>Choose your value of rate:</h2>
        <form name="rate_form" action="
<c:url value="/users/makerate.html?race=${param.race}&jokey=${param.jokey}&ratetype=${param.ratetype}"/>" method="post"
              onsubmit="return valid_rate();">
            <label for="summ"></label>
            <input type="number" min="${rate.getMinsumm()}" max="${client.getBalance()}" required class="form-control"
                   name="summ" id="summ" style="width: 50%; margin: 0 auto; text-align: center;"><br>
            <div class="btn-group">
                <button type="submit" class="btn btn-success">RATE!</button>
            </div>
        </form>
        <div class="box-stitches-bottom"></div>
    </div>
</my:body>
