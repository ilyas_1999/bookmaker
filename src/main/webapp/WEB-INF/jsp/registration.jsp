<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<my:body>
    <div class="box" style="left:50%; top:50%; width: 300px; height: 410px; margin: -205px auto 0 -150px;">
        <div class="box-stitches-top"></div>
        <form:form action="add.html" commandName="registrationForm">
            <div class="form-group" style="margin-bottom: 5px;">
                <label for="login"><h4>Login:</h4></label>
                <spring:bind path="User">
                <form:input class="form-control" id="login" path="login"
                            style="width: 50%; margin: 0 auto; text-align: center;"/>
                <form:errors path="login" cssClass="error"/>
            </div>
            <div class="form-group" style="margin-bottom: 5px;">
                <label for="password"><h4>Password:</h4></label>
                <form:password class="form-control" id="password" path="password"
                               style="width: 50%; margin: 0 auto; text-align: center;"/>
                <form:errors path="password" cssClass="error"/>
            </div>
                </spring:bind>
            <spring:bind path="Client">
            <div class="form-group" style="margin-bottom: 10px;">
                <label for="firstName"><h4>First name:</h4></label>
                <form:input class="form-control" id="firstName" path="firstName"
                            style="width: 50%; margin: 0 auto; text-align: center;"/>
                <form:errors path="firstName" cssClass="error"/>
            </div>
            <div class="form-group" style="margin-bottom: 5px;">
                <label for="passport"><h4>Passport:</h4></label>
                <form:input class="form-control" id="passport" path="passport"
                            style="width: 50%; margin: 0 auto; text-align: center;"/>
                <form:errors path="passport" cssClass="error"/>
            </div>
            </spring:bind>
            <div class="form-group">
                <button type="submit" class="btn btn-success">Enter</button>
            </div>
        </form:form>
        <div class="box-stitches-bottom"></div>
    </div>
</my:body>

