<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<my:body>
    <div class="box" style="margin-left: 120px; top: 50%; width: 40%;
    max-height: 336px; min-height: 336px; margin-top: -168px;">
        <div class="box-stitches-top"></div>
        <h2>Open races:</h2>
        <table class="table">
            <thead>
            <tr>
                <td>Name</td>
                <td>Date</td>
                <td>Time</td>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${current_races}" var="races">
                <tr onclick="window.document.location='/races/currentrace.html?name=${races.getName()}';" style="cursor: pointer;">
                    <td><c:out value="${races.getName()}"/></td>
                    <td><c:out value="${races.getDate()}"/></td>
                    <td><c:out value="${races.getTime()}"/></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <div class="box-stitches-bottom"></div>
    </div>

    <div class="box" style="margin-left: 52%; top: 50%; width: 40%;
    max-height: 336px; min-height: 336px; margin-top: -168px;">
        <div class="box-stitches-top"></div>
        <h2>Closed races:</h2>
        <table class="table">
            <thead>
            <tr>
                <td>Name</td>
                <td>Date</td>
                <td>Time</td>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${past_races}" var="races">
                <tr onclick="window.document.location='/races/pastrace.html?name=${races.getName()}';" style="cursor: pointer;">
                    <td><c:out value="${races.getName()}"/></td>
                    <td><c:out value="${races.getDate()}"/></td>
                    <td><c:out value="${races.getTime()}"/></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <div class="box-stitches-bottom"></div>
    </div>
</my:body>
