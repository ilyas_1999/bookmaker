<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<my:body>
    <div class="box" style="width: 45%;  height: 65%; left: 50%; top:50%; margin: -15% auto 0 -22%;">
        <div class="box-stitches-top"></div>
        <h2>All races:</h2>
        <form action="<c:url value="/races/delete.html"/>" method="post" onsubmit="return confirm_delete();">
            <table class="table">
                <thead>
                <tr>
                    <c:if test="${sessionScope.user_role==\"admin\"}">
                        <td></td>
                    </c:if>
                    <td>Name</td>
                    <td>Date</td>
                    <td>Time</td>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${all_races}" var="races">
                    <tr>
                        <c:if test="${sessionScope.user_role==\"admin\"}">
                            <td><input type="checkbox" value="${races.key}" name="checked_races" title="${races.key}">
                            </td>
                        </c:if>
                        <c:if test="${sessionScope.user_role==\"admin\"}">
                            <td><a href="<c:url value="/races/edit.html?id=${races.key}"/>"
                                   style="color: #000000; text-decoration: none;">
                                <c:out value="${races.getValue().getName()}"/></a></td>
                        </c:if>
                        <c:if test="${sessionScope.user_role!=\"admin\"}">
                            <td><c:out value="${races.getValue().getName()}"/></td>
                        </c:if>
                        <td><c:out value="${races.getValue().getDate()}"/></td>
                        <td><c:out value="${races.getValue().getTime()}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <c:if test="${sessionScope.user_role==\"admin\"}">
                <div class="btn-group">
                    <button type="submit" class="btn btn-success">Delete</button>
                    <input type="button" class="btn btn-info" value="Add"
                           onclick="location.href='/races/edit.html?id=-1'">
                </div>
            </c:if>
        </form>
        <div class="box-stitches-bottom"></div>
    </div>
</my:body>
