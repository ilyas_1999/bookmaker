<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<my:body>
    <div class="box" style="width: 40%;
    height: 65%; left: 50%; top:50%; margin: -15% auto 0 -20%;">
        <div class="box-stitches-top"></div>
        <h2>All users:</h2>
        <form action="<c:url value="/users/delete.html"/>" method="post" onsubmit="return confirm_delete();">
            <table class="table">
                <thead>
                <tr>
                    <td></td>
                    <td>Login</td>
                    <td>Password</td>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${all_users}" var="user">
                    <tr>
                        <td><input type="checkbox" value="${user.getLogin()}" name="checked_users"
                                   title="${user.getLogin()}" <c:if test="${user.getLogin() == \"admin\"}">
                                   disabled  </c:if>></td>
                        <c:if test="${user.getLogin() == \"admin\"}">
                            <td><c:out value="${user.getLogin()}"/></td>
                        </c:if>
                        <c:if test="${user.getLogin() != \"admin\"}">
                            <td>
                                <a href="<c:url value="/users/edit.html?l=${user.getLogin()}"/>"
                                   style="color: #000000; text-decoration: none;">
                                    <c:out value="${user.getLogin()}"/></a></td>
                        </c:if>
                        <td><c:out value="${user.getPassword()}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <div class="btn-group">
                <button type="submit" class="btn btn-success">Delete</button>
                <input type="button" class="btn btn-info" value="Add"
                       onclick="location.href='/users/edit.html?l=-1'">
            </div>
        </form>
        <div class="box-stitches-bottom"></div>
    </div>
</my:body>
