<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<my:body>
    <div class="box" style="top: 50%; width: 83%;
        max-height: 336px; min-height: 336px; margin-top: -168px; margin-left: 120px;">
        <div class="box-stitches-top"></div>
        <form action="<c:url value="/admin/composition.html?name=${param.name}"/>" method="post">
            <h2>Players:</h2>
        <table class="table">
        <c:forEach items="${all_players}" var="player">
            <tr>
            <td><input type="checkbox" name="check_players" title="${player.key}" value="${player.getValue().getJokey()}"></td>
                <td><c:out value="${player.getValue().getJokey()}"/></td>
            </tr>
        </c:forEach>
        </table>
    <div class="btn-group">
        <button type="submit" class="btn btn-success">Approve</button>
        <input type="button" class="btn btn-info" value="Back" onclick="location.href=history.back()">
    </div>
</form>
        <div class="box-stitches-bottom"></div>
    </div>
</my:body>
