<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<my:body>
    <div class="box" style="width: 300px;
     height: 65%; left: 50%; top:50%; margin: -15% auto 0 -150px;">
        <div class="box-stitches-top"></div>
            <c:set value="${param.j}" var="j"/>
            <form:form commandName="playerForm" action="edit.html?j=${j}">
            <label for="jokey"><h2>Jokey</h2></label>
    <form:input class="form-control" path="jokey" id="jokey"
           style="width: 50%; margin: 0 auto; text-align: center;"
           value="${linked_player.getJokey()}"/><br>
    <form:errors path="jokey" cssClass="error" />

            <label for="horse"><h2>Horse</h2></label>
    <form:input class="form-control" path="horse" id="horse"
           style="width: 50%; margin: 0 auto; text-align: center;"
           value="${linked_player.getHorse()}"/><br>
    <form:errors path="horse" cssClass="error" />
            <div class="btn-group">
    <button type="submit" class="btn btn-success">Save</button>
                <input type="button" class="btn btn-info" value="Back" onclick="location.href=history.back()">
            </div>
    </form:form>
            <div class="box-stitches-bottom"></div>
    </div>
</my:body>
