<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<my:body>
    <div class="box" style="width: 40%; height: 65%; left: 50%; top:50%; margin: -15% auto 0 -20%;">
        <div class="box-stitches-top"></div>
        <h2>Approve results:</h2>
        <form name="results" action="<c:url value="/admin/results.html?race=${param.race}"/>" method="post">
            <table class="table">
                <thead>
                <tr>
                    <td>Player</td>
                    <td>Place</td>
                </tr>
                </thead>
                <tbody>
            <c:forEach items="${players_in_race}" var="player" varStatus="loop">
            <tr>
                <td>
                <c:out value="${player.getJokey()}"/>
                </td>
                <td>
                <select name="${player.getJokey()}" class="mySelect" title="${loop.index}" onchange="disable_place(this);">
                    <option disabled selected value>place</option>
                    <c:forEach  var="i" begin="1" end="14">
                        <option value="${i}">${i}</option>
                    </c:forEach>
                </select>
                </td>
            </tr>
            </c:forEach>
                </tbody>
            </table>
            <div class="btn-group">
    <button type="submit" class="btn btn-success" disabled id="close">Close race</button>
                <input type="button" class="btn btn-primary" value="Refresh"
                       onclick="location.href ='/results.html?id=${param.id}'"/>
            </div>
        </form>
        <div class="box-stitches-bottom"></div>
    </div>
</my:body>
