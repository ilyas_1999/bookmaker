<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<my:body>
    <div class="box" style="width: 40%; height: 65%; left: 50%; top:50%; margin: -15% auto 0 -20%;">
        <div class="box-stitches-top"></div>
        <h2>Started races:</h2>
            <table class="table">
                <thead>
                <tr>
                    <td>Name</td>
                    <td>Date</td>
                    <td>Time</td>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${started_races}" var="races">
                    <tr>
                            <td><a href="<c:url value="/admin/results.html?race=${races.getName()}"/>" style="color: #000000; text-decoration: none;">
                                <c:out value="${races.getName()}"/></a></td>
                        <td><c:out value="${races.getDate()}"/></td>
                        <td><c:out value="${races.getTime()}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        <div class="box-stitches-bottom"></div>
    </div>
</my:body>

