<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<my:body>
    <div class="box" style="width: 55%;  min-height: 65%; left: 50%; top:50%; margin: -15% auto 0 -27%;">
        <div class="box-stitches-top"></div>
        <h2>The race ${linked_race.getName()} ends in ${linked_race.getDate()}</h2>
        <h2>Results:</h2>
            <table class="table">
                <thead>
                <tr>
                    <td>Player</td>
                    <td>Place</td>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${players}" var="players" varStatus="loop">
                    <tr>
                        <td><c:out value="${players.getJokey()}"/></td>
                        <td><c:out value="${results.get(loop.index).getPlace()}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        <div class="box-stitches-bottom"></div>
    </div>
</my:body>