<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<my:body>
    <div class="box" style="width: 40%;  height: 30%; left: 50%; top:50%; margin: -10% auto 0 -18%;">
        <div class="box-stitches-top"></div>
        <h2>Chosen race: ${param.race}, player: ${param.jokey}</h2>
        <h2>Choose the type of rate:</h2>
        <form name="rate_form" action="
<c:url value="/users/rate.html?race=${param.race}&jokey=${param.jokey}"/>" method="post">
            <label for="ratetype"><h4>Available rate types:</h4></label>
            <c:if test="${rate_types.isEmpty()}">
                <h4>No rates is available for this configuration!</h4>
                <div class="btn-group">
                    <input type="button" class="btn btn-info" value="Back" onclick="location.href=history.back()">
                </div>
            </c:if>
            <c:if test="${!rate_types.isEmpty()}">
            <select class="form-control" id="ratetype" name="ratetype" required style="width: 50%; margin: 0 auto; text-align: center;">
                <c:forEach items="${rate_types}" var="types">
                    <option><c:out value="${types.getName()}"/></option>
                </c:forEach>
            </select><br>
            <div class="btn-group">
                <button type="submit" class="btn btn-success">NEXT!</button>
            </div>
            </c:if>
        </form>
        <div class="box-stitches-bottom"></div>
    </div>
</my:body>
