<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<my:body>
    <div class="box" style="width: 40%; height: 65%; left: 50%; top:50%; margin: -15% auto 0 -20%;">
        <div class="box-stitches-top"></div>
        <h2>All tracks:</h2>
        <form action="<c:url value="/tracks/delete.html"/>" method="post" onsubmit="return confirm_delete();">
            <table class="table">
                <thead>
                <tr>
                    <c:if test="${sessionScope.user_role==\"admin\"}">
                        <td></td>
                    </c:if>
                    <td>Name</td>
                    <td>Type</td>
                    <td>City</td>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${all_tracks}" var="tracks">
                    <tr>
                        <c:if test="${sessionScope.user_role==\"admin\"}">
                            <td><input type="checkbox" value="${tracks.key}" name="checked_tracks"
                                       title="${tracks.key}"></td>
                        </c:if>
                        <c:if test="${sessionScope.user_role==\"admin\"}">
                            <td><a href="<c:url value="/tracks/edit.html?id=${tracks.key}"/>"
                                   style="color: #000000; text-decoration: none;">
                                <c:out value="${tracks.getValue().getName()}"/></a></td>
                        </c:if>
                        <c:if test="${sessionScope.user_role!=\"admin\"}">
                            <td><c:out value="${tracks.getValue().getName()}"/></td>
                        </c:if>
                        <td><c:out value="${tracks.getValue().getType()}"/></td>
                        <td><c:out value="${tracks.getValue().getCity()}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <c:if test="${sessionScope.user_role==\"admin\"}">
            <div class="btn-group">
                <button type="submit" class="btn btn-success">Delete</button>
                <input type="button" value="Add" class="btn btn-info" onclick="location.href='/tracks/edit.html?id=-1'">
            </div>
            </c:if>
            <div class="box-stitches-bottom"></div>
    </div>
</my:body>