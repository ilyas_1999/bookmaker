<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<my:body>
    <div class="box" style="width: 40%; left: 50%; top:50%; margin: -15% auto 0 -20%;
     min-height: 65%; max-height: 65%;">
        <div class="box-stitches-top"></div>
        <h2>Approved races:</h2>
            <table class="table">
                <thead>
                <tr>
                    <td>Name</td>
                    <td>Date</td>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${races}" var="race">
                    <tr>
                            <td><a href="<c:url value="/bookmaker/rateplayers.html?name=${race.getName()}"/>"
                                   style="color: #000000; text-decoration: none;">
                                <c:out value="${race.getName()}"/></a></td>
                        <td><c:out value="${race.getDate()}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        <div class="box-stitches-bottom"></div>
    </div>
</my:body>
