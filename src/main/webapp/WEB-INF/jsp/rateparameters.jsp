<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<my:body>
    <div class="box" style="width: 30%;  height: 70%; left: 50%; top:50%; margin: -15% auto 0 -17%;">
        <div class="box-stitches-top"></div>
        <h2>Chosen race: ${param.race}</h2>
        <h2>Chosen player: ${param.jokey}</h2>
        <form name="rate_form" action="
<c:url value="/bookmaker/rateparameters.html?race=${param.race}&jokey=${param.jokey}"/>" method="post"
              onsubmit="return valid_rate();">
            <label for="maxcount"><h4>Max count:</h4></label>
            <input type="number" min="1" class="form-control" name="maxcount" id="maxcount" style="width: 50%; margin: 0 auto; text-align: center;">
            <label for="minsumm"><h4>Min summ:</h4></label>
            <input type="number" min="1" class="form-control" name="minsumm" id="minsumm" style="width: 50%; margin: 0 auto; text-align: center;">
            <label for="maxsumm"><h4>Max summ:</h4></label>
            <input type="number" min="1" class="form-control" name="maxsumm" id="maxsumm" style="width: 50%; margin: 0 auto; text-align: center;">
            <label for="coeff"><h4>Coefficient:</h4></label>
            <input type="number" min="0" step="any" class="form-control" name="coeff" id="coeff" style="width: 50%; margin: 0 auto; text-align: center;">
            <label for="selected_rate_type"><h4>Rate type:</h4></label>
            <select class="form-control" id="selected_rate_type" name="selected_rate_type" required style="width: 50%; margin: 0 auto; text-align: center;">
                <c:forEach items="${rate_types}" var="types">
                    <option><c:out value="${types.getValue().getName()}"/></option>
                </c:forEach>
            </select><br>
            <div class="btn-group">
                <button type="submit" class="btn btn-success">Add</button>
            </div>
        </form>
        <div class="box-stitches-bottom"></div>
    </div>
</my:body>
