<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<my:body>
    <div class="box" style="width: 40%; left: 50%; top:50%; margin: -15% auto 0 -20%;
     min-height: 65%; max-height: 65%;">
        <div class="box-stitches-top"></div>
        <h2>All players:</h2>
        <form action="<c:url value="/players/delete.html"/>" method="post" onsubmit="return confirm_delete();">
            <table class="table">
                <thead>
                <tr>
                    <c:if test="${sessionScope.user_role==\"admin\"}">
                        <td></td>
                    </c:if>
                    <td>Jokey name</td>
                    <td>Horse</td>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${all_players}" var="players">
                    <tr>
                        <c:if test="${sessionScope.user_role==\"admin\"}">
                            <td><input type="checkbox" value="${players.getJokey()}" name="checked_players"></td>
                        </c:if>
                        <c:if test="${sessionScope.user_role==\"admin\"}">
                            <td><a href="<c:url value="/players/edit.html?j=${players.getJokey()}"/>"
                                   style="color: #000000; text-decoration: none;">
                                <c:out value="${players.getJokey()}"/></a></td>
                        </c:if>
                        <c:if test="${sessionScope.user_role!=\"admin\"}">
                            <td><c:out value="${players.getJokey()}"/></td>
                        </c:if>
                        <td><c:out value="${players.getHorse()}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <c:if test="${sessionScope.user_role==\"admin\"}">
                <div class="btn-group">
                    <button type="submit" class="btn btn-success">Delete</button>
                    <input type="button" class="btn btn-info" value="Add"
                           onclick="location.href='/players/edit.html?j=-1'"/>
                </div>
            </c:if>
        </form>
        <div class="box-stitches-bottom"></div>
    </div>
</my:body>