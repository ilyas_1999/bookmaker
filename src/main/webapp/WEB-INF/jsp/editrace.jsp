<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<my:body>
    <div class="box" style="width: 380px;
    height: 68%; left: 50%; top:50%; margin: -15% auto 0 -190px;">
        <div class="box-stitches-top"></div>
        <c:set var="id" value="${param.id}"/>
            <form:form commandName="raceForm" action="edit.html?id=${id}">
                <label for="name"><h4>Name</h4></label>
                <form:input class="form-control" path="name" id="name"
                       value="${linked_race.getName()}"
                       style="width: 50%; margin: 0 auto; text-align: center;"/>
                <form:errors path="name" cssClass="error" /><br>
                    <label for="date"><h4>Date</h4></label>
                <form:input type="date" class="form-control" path="date" id="date"
                       value="${linked_race.getDate()}"
                       style="width: 50%; margin: 0 auto; text-align: center;"/>
                <form:errors path="date" cssClass="error" /><br>
                    <label for="time"><h4>Time</h4></label>
                    <form:input type="time" class="form-control" path="time" id="time"
                           value="${linked_race.getTime()}"
                           style="width: 50%; margin: 0 auto; text-align: center;"/>
                <form:errors path="time" cssClass="error" /><br>
                    <label for="selected_track"><h4>Track</h4></label>
                    <select class="form-control" id="selected_track" name="selected_track" required title="tracks"
                            style="width: 50%; margin: 0 auto; text-align: center;">
                        <c:forEach items="${alltracks}" var="tracks">
                            <option><c:out value="${tracks.getValue().getName()}"/></option>
                        </c:forEach>
                    </select><br>
                    <div class="btn-group">
                        <button type="submit" class="btn btn-success">Save</button>
                        <input type="button" class="btn btn-info" value="Back" onclick="location.href=history.back()">
                    </div>
            </form:form>
                <div class="box-stitches-bottom"></div>
    </div>
</my:body>
