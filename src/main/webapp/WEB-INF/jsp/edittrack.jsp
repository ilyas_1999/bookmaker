<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<my:body>
    <div class="box" style="width: 300px; height: 65%; left: 50%; top:50%; margin: -15% auto 0 -150px;">
        <div class="box-stitches-top"></div>
        <c:set var="id" value="${param.id}"/>
            <form:form commandName="trackForm" action="edit.html?id=${id}">
                <label for="name"><h4>Name</h4></label>
                <form:input class="form-control" path="name" id="name"
                       value="${linked_track.getName()}"
                       style="width: 50%; margin: 0 auto; text-align: center;"/>
                <form:errors path="name" cssClass="error" /><br>
                    <label for="type"><h4>Type</h4></label>
                    <form:input class="form-control" path="type" id="type"
                           value="${linked_track.getType()}"
                           style="width: 50%; margin: 0 auto; text-align: center;"/>
                <form:errors path="type" cssClass="error" /><br>
                <label for="city"><h4>City</h4></label>
                <form:input class="form-control" path="city" id="city"
                       value="${linked_track.getCity()}"
                       style="width: 50%; margin: 0 auto; text-align: center;"/>
                <form:errors path="city" cssClass="error" /><br>
                    <div class="btn-group">
                        <button type="submit" class="btn btn-success">Save</button>
                        <input type="button" class="btn btn-info" value="Back" onclick="location.href=history.back()">
                    </div>
            </form:form>
                <div class="box-stitches-bottom"></div>
    </div>
</my:body>
