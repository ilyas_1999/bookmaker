<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<my:body>
    <div class="box" style="width: 330px; min-height: 45%; left:50%; top:50%; margin: -15% auto 0 -350px;">
        <div class="box-stitches-top"></div>
        <form action="<c:url value="/users/cabinet/save.html?login=${param.login}"/>" name="user_form"
              method="post" onsubmit="return valid_cabinet();">
            <div class="form-group" style="margin-bottom: 5px;">
                <label for="fname"><h4>First name:</h4></label>
                <input type="text" class="form-control" id="fname" name="fname"
                       value="${linked_client.getFirstName()}"
                       style="width: 50%; margin: 0 auto; text-align: center;">
            </div>
            <div class="form-group" style="margin-bottom: 5px;">
                <label for="sname"><h4>Second name:</h4></label>
                <input type="text" class="form-control" id="sname" name="sname"
                       value="${linked_client.getSecondName()}"
                       style="width: 50%; margin: 0 auto; text-align: center;">
            </div>
            <div class="form-group" style="margin-bottom: 5px;">
                <label for="passport"><h4>Passport:</h4></label>
                <input type="text" class="form-control" id="passport" name="passport"
                       value="${linked_client.getPassport()}"
                       style="width: 50%; margin: 0 auto; text-align: center;">
            </div>
            <div class="form-group" style="margin-bottom: 5px;">
                <label for="balance"><h4>Balance:</h4></label>
                <input type="number" class="form-control" id="balance" name="balance" readonly
                       value="${linked_client.getBalance()}"
                       style="width: 50%; margin: 0 auto; text-align: center;">
            </div>
            <div class="btn-group">
                <button type="submit" class="btn btn-success">Save</button>
                <input type="button" class="btn btn-info" value="Up balance"
                       onclick="location.href ='/users/upbalance.html?login=${param.login}'"/>
            </div>
        </form>
        <div class="box-stitches-bottom"></div>
    </div>
    <div class="box" style="width: 550px; min-height: 45%; left:50%; top:50%; margin: -15% auto 0 0;">
        <div class="box-stitches-top"></div>
        <h2>Your rates:</h2>
        <table class="table">
            <thead>
            <tr>
                <td>Race</td>
                <td>Player</td>
                <td>Type</td>
                <td>Summ</td>
                <td>Status</td>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${rates}" var="rates" varStatus="loop">
                <tr>
                    <td><c:out value="${rates.getRace().getName()}"/></td>
                    <td><c:out value="${rates.getPlayer().getJokey()}"/></td>
                    <td><c:out value="${rates.getRateType().getName()}"/></td>
                    <td><c:out value="${amounts.get(loop.index)}"/></td>
                    <td><c:out value="${statuses.get(loop.index)}"/></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <div class="box-stitches-bottom"></div>
    </div>
</my:body>
