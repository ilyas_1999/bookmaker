<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<my:body>
    <div class="box" style="width: 55%;  min-height: 65%; left: 50%; top:50%; margin: -15% auto 0 -27%;">
        <div class="box-stitches-top"></div>
        <h2>The race "${linked_race.getName()}" started at ${linked_race.getDate()}</h2>
        <h2>Players in the race:</h2>
        <table class="table">
            <thead>
            <tr>
                <td>Jokey name</td>
                <td>Horse</td>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${players_in_race}" var="players">
                <tr <c:if test="${sessionScope.user_role!=\"client\"}">
                        data-toggle="popover" data-container="body" data-placement="left"
                data-content="To make rate you need to be signed!"</c:if>>
                <c:if test="${sessionScope.user_role!=\"client\"}">
                    <td>
                       <c:out value="${players.getJokey()}"/></td>
                </c:if>
                <c:if test="${sessionScope.user_role==\"client\"}">
                    <td><a href="<c:url value="/users/ratetype.html?race=${linked_race.getName()}&jokey=${players.getJokey()}"/>" style="color: #000000; text-decoration: none;">
                        <c:out value="${players.getJokey()}"/></a></td>
                </c:if>
                    <td><c:out value="${players.getHorse()}"/></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <div class="box-stitches-bottom"></div>
    </div>
</my:body>