<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<my:body>
    <div class="box" style="width: 55%;  min-height: 65%; left: 50%; top:50%; margin: -15% auto 0 -27%;">
        <div class="box-stitches-top"></div>
    <h2>Sorry, you can't rate this race anymore!</h2><br>
        <div class="box-stitches-bottom"></div>
    </div>
</my:body>