<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<my:body>
    <div class="box" style="width: 300px; height: 65%; left: 50%; top:50%; margin: -15% auto 0 -150px;">
        <div class="box-stitches-top"></div>
        <c:set var="l" value="${param.l}"/>
            <form:form commandName="userForm" action="edit.html?l=${l}">
                <label for="login"><h4>Login</h4></label>
                <form:input class="form-control" path="login" id="login"
                       value="${linked_user.getLogin()}"
                       style="width: 50%; margin: 0 auto; text-align: center;"/>
                <form:errors path="login" cssClass="error" /><br>
                <label for="password"><h4>Password</h4></label>
                <form:input class="form-control" path="password" id="password"
                       value="${linked_user.getPassword()}"
                       style="width: 50%; margin: 0 auto; text-align: center;"/>
                <form:errors path="password" cssClass="error" /><br>
                    <label for="selected_role"><h4>Role</h4></label>
                <select class="form-control" id="selected_role" name="role"
                        style="width: 50%; margin: 0 auto; text-align: center;">
                    <c:forEach items="${allroles}" var="roles">
                        <c:if test="${roles.getValue().getName()==linked_user_role}">
                        <option value="${roles.getValue().getName()}"
                        selected="selected">${roles.getValue().getName()}</option>
                        </c:if>
                        <c:if test="${roles.getValue().getName()!=linked_user_role}">
                            <option value="${roles.getValue().getName()}">${roles.getValue().getName()}</option>
                        </c:if>
                    </c:forEach>
                </select><br>
                    <div class="btn-group">
                        <button type="submit" class="btn btn-success">Save</button>
                        <input type="button" class="btn btn-info" value="Back" onclick="location.href=history.back()">
                    </div>
            </form:form>
                <div class="box-stitches-bottom"></div>
    </div>
</my:body>