<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<my:body>
    <div class="box" style="left:50%; top:50%; width: 300px;
     height: 370px; margin: -185px auto 0 -150px;">
        <div class="box-stitches-top"></div>
        <form:form action="login.html" modelAttribute="loginForm">
            <div class="form-group" style="margin-bottom: 30px;">
                <label for="login"><h2>Login:</h2></label>
                <form:input class="form-control" id="login" path="login"
                       style="width: 50%; margin: 0 auto; text-align: center;"/>
                <form:errors path="login" cssClass="error" />
            </div>

            <div class="form-group" style="margin-bottom: 30px;">
                <label for="password"><h2>Password:</h2></label>
                <form:password class="form-control" id="password" path="password"
                       style="width: 50%; margin: 0 auto; text-align: center;"/>
                <form:errors path="password" cssClass="error" />
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Enter"/>
            </div>
        </form:form>
        <div class="box-stitches-bottom"></div>
    </div>
</my:body>
