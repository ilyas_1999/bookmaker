<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<nav class="navbar navbar-inverse" style="font-family: 'Helvetica Neue', Helvetica, sans-serif;
 background: #D3D15E;">
    <div class="container-fluid">
        <ul class="nav navbar-nav" >
            <li>
                <div class="ribbon"><div class="ribbon-stitches-top"></div>
                <strong class="ribbon-content"><h1>Bookmayker</h1></strong>
                            <div class="ribbon-stitches-bottom"></div></div></li>
            <li><a href="<c:url value="/main.html"/>"><span style="color: #801111"><b> Start!</b></span></a></li>
           <li><a href="<c:url value="/players/all.html"/>"><span style="color: #801111"><b>Players</b></span></a></li>
                <li><a href="<c:url value="/races/all.html"/>"><span style="color: #801111"><b>Races</b></span></a></li>
                <li><a href="<c:url value="/tracks/all.html"/>"><span style="color: #801111"><b>Tracks</b></span></a></li>
            <c:if test="${sessionScope.user_role==\"admin\"}">
                <li><a href="<c:url value="/users/all.html"/>"><span style="color: #801111"><b>Users</b></span></a></li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span style="color: #801111">
                    <b>Administration</b></span>
                    <span class="caret"></span></a>
                <ul class="dropdown-menu" style="background: #D3D15E;">
                    <li><a href="<c:url value="/races/futureraces.html"/>"><span style="color: #801111">
                        Composition</span></a></li>
                    <li><a href="<c:url value="/races/startedrace.html"/>"><span style="color: #801111">
                        Results</span></a></li>
                </ul>
            </li>
        </c:if>
            <c:if test="${sessionScope.user_role==\"bookmaker\"}">
                <li><a href="<c:url value="/races/approvedraces.html"/>"><span style="color: #801111"><b>Bookmaking</b></span></a></li>
            </c:if>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <c:if test="${sessionScope.user_role==\"client\"}">
            <li><a href="<c:url value="/users/cabinet.html?login=${sessionScope.user.getLogin()}"/>">
                <span style="color: #801111"><b>Hi, ${sessionScope.user.getLogin()}</b></span></a></li>
            </c:if>
            <li><a href="<c:url value="/users/add.html"/>"><span class="glyphicon glyphicon-user"
                                                                    style="color: #801111"></span>
                <span style="color: #801111"><b>Sign Up</b></span></a></li>
            <li><a href="<c:url value="/users/login.html"/>"><span class="glyphicon glyphicon-log-in"
                                                             style="color: #801111"></span>
                <span style="color: #801111"><b>Login</b></span></a></li>

        </ul>
    </div>
</nav>