<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="<c:url value="/resources/js/script.js"/>"></script>
<link rel="stylesheet" href="<c:url value="/resources/css/style.css"/>">
<html>
<head>
    <my:header/>
</head>
<body>

<div class="alert alert-success" id="added" style="width: 50%; margin: 0 auto;" hidden>
    <strong>Added successfully!</strong>
</div>
<div class="alert alert-success" id="edited" style="width: 50%; margin: 0 auto;" hidden>
    <strong>Changed successfully!</strong>
</div>
<div class="alert alert-success" id="deleted" style="width: 50%; margin: 0 auto;" hidden>
    <strong>Deleted successfully!</strong>
</div>
<div class="alert alert-danger" id="empty_fields" style="width: 50%; margin: 0 auto;" hidden>
    <strong>Fields can't be empty!</strong>
</div>
<div class="alert alert-danger" id="wrong_authorization" style="width: 50%; margin: 0 auto;" hidden>
    <strong>Wrong login/password</strong>
</div>
<div class="alert alert-success" id="registered" style="width: 50%; margin: 0 auto;" hidden>
    <strong>Success registration!</strong>
</div>
<div class="alert alert-warning" id="exist_login" style="width: 50%; margin: 0 auto;" hidden>
    <strong>Such login is already exists!</strong>
</div>
<div class="alert alert-danger" id="wrong_authorization" style="width: 50%; margin: 0 auto;" hidden>
    <strong>Wrong login/password</strong>
</div>
<div class="alert alert-danger" id="false_delete" style="width: 50%; margin: 0 auto;" hidden>
    <strong>Deleting failed!</strong>
</div>
<c:choose>
    <c:when test="${param.successDeleted}">
        <script>
            show_message("#deleted");
        </script>
    </c:when>
    <c:when test="${param.successAdded}">
        <script>
            show_message("#added");
        </script>
    </c:when>
    <c:when test="${param.successEdited}">
        <script>
            show_message("#edited");
        </script>
    </c:when>
    <c:when test="${param.successLogined==false}">
        <script>
            wrong_fields_in_login();
        </script>
    </c:when>
    <c:when test="${param.successRegistered}">
        <script>
            show_message("#registered");
        </script>
    </c:when>
    <c:when test="${param.successRegistered==false}">
        <script>
            show_message("#exist_login");
        </script>
    </c:when>
    <c:when test="${param.successDeleted==false}">
        <script>
            show_message("#false_delete");
        </script>
    </c:when>
    <c:when test="${param.emptyFields==true}">
        <script>
            show_message("#empty_fields");
        </script>
    </c:when>
</c:choose>
    <jsp:doBody/>
</body>
</html>