package com.goodsoft.bookmaker.storage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Administrator.
 */
public class JdbcConnector {
    private Connection connection = null;

    public JdbcConnector() throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        this.connection = DriverManager
                .getConnection("jdbc:postgresql://localhost:5432/Practice", "postgres", /*"5672678"*/"postgres");
    }

    public Connection getConnection() {
        return connection;
    }
}

