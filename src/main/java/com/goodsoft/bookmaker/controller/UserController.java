package com.goodsoft.bookmaker.controller;

import com.goodsoft.bookmaker.entity.account.Client;
import com.goodsoft.bookmaker.entity.account.Role;
import com.goodsoft.bookmaker.entity.account.User;
import com.goodsoft.bookmaker.entity.contora.ClientsRates;
import com.goodsoft.bookmaker.entity.contora.Race;
import com.goodsoft.bookmaker.entity.contora.Rate;
import com.goodsoft.bookmaker.service.account.client.ClientService;
import com.goodsoft.bookmaker.service.account.role.RoleService;
import com.goodsoft.bookmaker.service.account.rolesusers.RolesUsersService;
import com.goodsoft.bookmaker.service.account.user.UserService;
import com.goodsoft.bookmaker.service.contora.clientsrates.ClientsRatesService;
import com.goodsoft.bookmaker.service.contora.player.PlayerService;
import com.goodsoft.bookmaker.service.contora.race.RaceService;
import com.goodsoft.bookmaker.service.contora.rate.RateService;
import com.goodsoft.bookmaker.service.contora.ratetype.RateTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Administrator.
 */
@Controller
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private RolesUsersService rolesUsersService;
    @Autowired
    private ClientService clientService;
    @Autowired
    private ClientsRatesService clientsRatesService;
    @Autowired
    private RaceService raceService;
    @Autowired
    private PlayerService playerService;
    @Autowired
    private RateTypeService rateTypeService;
    @Autowired
    private RateService rateService;

    @RequestMapping(value = "/all.html", method = RequestMethod.GET)
    public String printAll(Model model) throws SQLException, ClassNotFoundException {

        model.addAttribute("all_users", userService.readAllUsers());
        return "users";
    }

    @RequestMapping(value = "/edit.html", method = RequestMethod.GET)
    public String edit(Model model, @RequestParam("l") String login) throws SQLException, ClassNotFoundException {
        model.addAttribute("allroles", roleService.readAllRoles());
        if (login.equals("-1")) {
            User user = new User();
            model.addAttribute("userForm", user);
            return "edituser";
        } else {
            User user = userService.findUserById(userService.findIdByLogin(login));
            String role = rolesUsersService.findNameByUserId(userService.findIdByLogin(login));
            model.addAttribute("linked_user", user);
            model.addAttribute("linked_user_role", role);
            model.addAttribute("userForm", user);
            return "edituser";
        }
    }

    @RequestMapping(value = "/edit.html", method = RequestMethod.POST)
    public String change(@RequestParam("l") String login,
                         @Valid @ModelAttribute("userForm") User user, BindingResult result,
                         @RequestParam("role") String role, Model model) throws SQLException, ClassNotFoundException {
        if (result.hasErrors()) {
            model.addAttribute("allroles", roleService.readAllRoles());
            return "edituser";
        } else {
            Role newRole = new Role();
            newRole.setName(role);
            if (login.equals("-1")) {
                if (!userService.existLogin(user.getLogin())) {
                    if (newRole.getName().equals("client")) {
                        Client newClient = new Client();
                        newClient.setFirstName("Name");
                        newClient.setPassport("VM");
                        newClient.setBalance(0D);
                        clientService.addClient(newClient, user);
                    } else {
                        userService.addUser(user);
                        rolesUsersService.addUserToRole(user, newRole);
                    }
                    return "redirect:/users/all.html?successAdded=true";
                } else {
                    return "redirect:/users/edit.html?l=-1&successAdded=false";
                }
            } else {
                if (!userService.existLogin(user.getLogin()) ||
                        Objects.equals(userService.findUserById(userService.findIdByLogin(login)).getLogin(), user.getLogin())) {
                    userService.editUser(userService.findIdByLogin(login), user);
                    rolesUsersService.deleteUserFromRole(userService.findIdByLogin(user.getLogin()));
                    rolesUsersService.addUserToRole(user, newRole);
                    return "redirect:/users/all.html?successEdited=true";
                } else {
                    return "/users/edit.html?l=" + login + "&successEdited=false";
                }
            }
        }
    }

    @RequestMapping(value = "/delete.html", method = RequestMethod.POST)
    public String delete(@RequestParam(value = "checked_users", required = false) String[] cu) throws SQLException, ClassNotFoundException {
        try {
            for (String aCu : cu) {
                rolesUsersService.deleteUserFromRole(userService.findIdByLogin(aCu));
                userService.deleteUser(userService.findUserById(Long.parseLong(aCu)));
            }
            return "redirect:/users/all.html?successDeleted=true";
        } catch (NullPointerException e) {
            return "redirect:/users/all.html?successDeleted=false";
        }
    }

    @RequestMapping(value = "/add.html", method = RequestMethod.GET)
    public String signUp(ModelMap model)
            throws ServletException, IOException, SQLException, ClassNotFoundException, ParseException {
        User user = new User();
        Client client = new Client();
        model.addAttribute("registrationForm", user);
        model.addAttribute("registrationForm", client);
        return "registration";
    }

    @RequestMapping(value = "/add.html", method = RequestMethod.POST)        //TODO validation method
    public String register(@Valid @ModelAttribute("User") User user, BindingResult result,
                           @Valid @ModelAttribute("Client") Client client, BindingResult result2)
            throws ServletException, IOException, SQLException, ClassNotFoundException {
        if (result.hasErrors() || result2.hasErrors()) {
            return "registration";
        } else {
            if (!userService.existLogin(user.getLogin())) {
                clientService.addClient(client, user);
                return "redirect:/main.html?successRegistered=true";
            } else {
                return "redirect:/registration.html?successRegistered=false";
            }
        }
    }

    @RequestMapping(value = "/login.html", method = RequestMethod.GET)
    public String logIn(HttpSession session, ModelMap model)
            throws ServletException, IOException, SQLException, ClassNotFoundException, ParseException {
        session.invalidate();
        User user = new User();
        model.addAttribute("loginForm", user);
        return "login";
    }

    @RequestMapping(value = "/login.html", method = RequestMethod.POST)
    public String entering(@Valid @ModelAttribute("loginForm") User user, BindingResult result, HttpSession session)
            throws ServletException, IOException, SQLException, ClassNotFoundException {
        if (result.hasErrors()) {
            return "login";
        } else {
            if (userService.existLogin(user.getLogin())) {
                if (userService.findUserById(userService.findIdByLogin(user.getLogin())).getPassword().equals(user.getPassword())) {
                    session.setAttribute("user", user);
                    session.setAttribute("user_role", rolesUsersService.findNameByUserId(userService.findIdByLogin(user.getLogin())));
                    return "redirect:/main.html";
                } else {
                    return "redirect:/users/login.html?successLogined=false";
                }
            } else {
                return "redirect:/users/login.html?successLogined=false";
            }
        }
    }

    @RequestMapping(value = "/cabinet.html", method = RequestMethod.GET)
    public String cabinet(Model model, @RequestParam("login") String login)
            throws ServletException, IOException, SQLException, ClassNotFoundException, ParseException {
        Client client = clientService.findClientByUserId(
                userService.findIdByLogin(login));
        model.addAttribute("linked_client", client);
        List<Rate> rates = new ArrayList<>();
        List<Double> amounts = new ArrayList<>();
        List<String> statuses = new ArrayList<>();
        for (ClientsRates clientsRates : clientsRatesService.findClientRateByClient(client)) {
            amounts.add(clientsRates.getAmount());
            rates.add(clientsRates.getRate());
            if (raceService.readClosedRaces().contains(clientsRates.getRate().getRace())) {
                statuses.add("payed");
            } else {
                statuses.add("waited");
            }
        }
        model.addAttribute("rates", rates);
        model.addAttribute("amounts", amounts);
        model.addAttribute("statuses", statuses);
        return "cabinet";
    }

    @RequestMapping(value = "/cabinet/upbalance.html", method = RequestMethod.POST)
    public String save(@RequestParam("login") String login)
            throws ServletException, IOException, SQLException, ClassNotFoundException {
        clientService.upBalance(clientService.findClientByUserId(userService.findIdByLogin(login)), 10000D);
        return "redirect:/users/cabinet.html?login=" + login;
    }

    @RequestMapping(value = "/cabinet/save.html", method = RequestMethod.POST)
    public String save(@Valid @ModelAttribute Client client, BindingResult result,
                       @RequestParam("login") String login)
            throws ServletException, IOException, SQLException, ClassNotFoundException {
        if(result.hasErrors()){
            return "cabinet";
        }
        else {
            clientService.editClient(clientService.findIdByPassport(
                    clientService.findClientByUserId(userService.findIdByLogin(login)).getPassport()),client);
            return "redirect:/main.html?successEdited=true";
        }
    }

    @RequestMapping(value = "/ratetype.html", method = RequestMethod.GET)
    public String rateType(Model model, @RequestParam("race") String race, @RequestParam("jokey") String jokey)
            throws ServletException, IOException, SQLException, ClassNotFoundException, ParseException {
        model.addAttribute("rate_types", rateTypeService.readRateTypesByRaceAndPlayer(
                raceService.findRaceById(raceService.findIdByName(race)),
                playerService.findPlayerById(playerService.findIdByJokey(jokey))));
        return "ratetype";
    }

    @RequestMapping(value = "/rate.html", method = RequestMethod.POST)
    public String rate(Model model, @RequestParam Map<String, String> params, HttpSession session)
            throws ServletException, IOException, SQLException, ClassNotFoundException, ParseException {
        Rate rate = rateService.findRateById(rateService.findIdByAll(
                playerService.findIdByJokey(params.get("jokey")),
                raceService.findIdByName(params.get("race")),
                rateTypeService.findIdByName(params.get("ratetype"))));
        Client client = clientService.findClientByUserId(
                userService.findIdByLogin(((User) session.getAttribute("user")).getLogin()));
        model.addAttribute("rate", rate);
        model.addAttribute("possible", new DecimalFormat("#.##").format(rate.getCoefficient()));
        model.addAttribute("client", client);
        model.addAttribute("ratetype", params.get("ratetype"));
        return "rate";
    }

    @RequestMapping(value = "/makerate.html", method = RequestMethod.POST)
    public String makeRate(@RequestParam Map<String, String> params, HttpSession session)
            throws ServletException, IOException, SQLException, ClassNotFoundException, ParseException, InvocationTargetException {
        Rate rate = rateService.findRateById(rateService.findIdByAll(playerService.findIdByJokey(params.get("jokey")),
                raceService.findIdByName(params.get("race")),
                rateTypeService.findIdByName(params.get("ratetype"))));
        Client client = clientService.findClientByUserId(userService.findIdByLogin(
                ((User) session.getAttribute("user")).getLogin()));
        Race raceEntity = raceService.findRaceById(raceService.findIdByName(params.get("race")));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        if (rate.getMaxcount() - clientsRatesService.findCountOfRates(rate) > 0) {
            if (rate.getMinsumm() <= Long.parseLong(params.get("summ")) && rate.getMaxsumm() >= Long.parseLong(params.get("summ")) && (Calendar.getInstance().getTimeInMillis()
                    - simpleDateFormat.parse(raceEntity.getDate() + " " + raceEntity.getTime()).getTime()) / 1000 < 0) {
                if (clientsRatesService.existRate(client, rate, Double.parseDouble((params.get("summ"))))) {
                    return "redirect:/main.html?successAdded=true";
                } else {
                    clientsRatesService.addRateToClient(client, rate,
                            Double.parseDouble(params.get("summ")));
                    return "redirect:/main.html?successAdded=true";
                }
            } else {
                return "redirect:/users/rate.html?race=" + params.get("race") + "&jokey=" + params.get("jokey")
                        + "&successRated=incorrect" + rate.getMinsumm() + "&" + rate.getMaxsumm()
                        + "&" + params.get("summ");
            }
        } else {
            return "outofrates";
        }
    }

}
