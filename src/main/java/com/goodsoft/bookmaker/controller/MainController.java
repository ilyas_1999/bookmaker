package com.goodsoft.bookmaker.controller;

import com.goodsoft.bookmaker.service.contora.race.RaceServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

/**
 * Created by Administrator.
 */
@Controller
@RequestMapping("/main.html")
public class MainController {

    @Autowired
    private RaceServiceImpl raceService;

    @RequestMapping(method = RequestMethod.GET)
    public String PrintForm(Model model)
            throws ServletException, IOException, SQLException, ClassNotFoundException, ParseException {
        model.addAttribute("current_races",raceService.readApprovedRaces());
        model.addAttribute("past_races",raceService.readClosedRaces());
        return "main";
    }

}
