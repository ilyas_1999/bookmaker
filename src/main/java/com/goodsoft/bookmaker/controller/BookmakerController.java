package com.goodsoft.bookmaker.controller;

import com.goodsoft.bookmaker.entity.contora.Rate;
import com.goodsoft.bookmaker.service.contora.player.PlayerService;
import com.goodsoft.bookmaker.service.contora.playersraces.PlayersRacesService;
import com.goodsoft.bookmaker.service.contora.race.RaceService;
import com.goodsoft.bookmaker.service.contora.rate.RateService;
import com.goodsoft.bookmaker.service.contora.ratetype.RateTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Map;

/**
 * Created by Administrator.
 */
@Controller
@RequestMapping("/bookmaker")
public class BookmakerController {

    @Autowired
    private PlayersRacesService playersRacesService;
    @Autowired
    private RateService rateService;
    @Autowired
    private RaceService raceService;
    @Autowired
    private PlayerService playerService;
    @Autowired
    private RateTypeService rateTypeService;

    @RequestMapping(value = "/rateplayers.html", method = RequestMethod.GET)
    public String ratePlayers(Model model, @RequestParam("name") String name) throws SQLException, ClassNotFoundException, ParseException {
        model.addAttribute("players", playersRacesService.readPlayersOnRace(
                raceService.findRaceById(raceService.findIdByName(name))));
        return "rateplayers";
    }

    @RequestMapping(value = "/rateparameters.html", method = RequestMethod.GET)
    public String ratePlayers(Model model) throws SQLException, ClassNotFoundException, ParseException {
        model.addAttribute("rate_types", rateTypeService.readAllRateTypes());
        return "rateparameters";
    }

    @RequestMapping(value = "/rateparameters.html", method = RequestMethod.POST)
    public String ratePlayers(@RequestParam Map<String, String> params) throws SQLException, ClassNotFoundException, ParseException {
        Rate rate = new Rate(Double.parseDouble(params.get("coeff")), Integer.parseInt(params.get("maxcount")),
                Double.parseDouble(params.get("minsumm")), Double.parseDouble(params.get("maxsumm")),
                rateTypeService.findRateTypeById(rateTypeService.findIdByName(params.get("selected_rate_type"))),
                raceService.findRaceById(raceService.findIdByName(params.get("race"))),
                playerService.findPlayerById(playerService.findIdByJokey(params.get("jokey"))));
        try {
            if (!rateService.existRate(rate) && rate.getMaxsumm() >= rate.getMinsumm()) {
                rateService.addRate(rate);
                return "redirect:/main.html?successAdded=true";
            } else {
                return "redirect:/bookmaker/rateparameters.html?successAdded=false";
            }
        } catch (NullPointerException e) {
            return "redirect:/bookmaker/rateparameters.html?successAdded=false";
        }
    }

}
