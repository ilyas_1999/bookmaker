package com.goodsoft.bookmaker.controller;

import com.goodsoft.bookmaker.entity.contora.Race;
import com.goodsoft.bookmaker.service.contora.playersraces.PlayersRacesService;
import com.goodsoft.bookmaker.service.contora.race.RaceService;
import com.goodsoft.bookmaker.service.contora.track.TrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletException;
import javax.validation.Valid;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Objects;

/**
 * Created by Administrator.
 */
@Controller
@RequestMapping("/races")
public class RaceController {

    @Autowired
    private RaceService raceService;
    @Autowired
    private PlayersRacesService playersRacesService;
    @Autowired
    private TrackService trackService;

    @RequestMapping(value = "/startedrace.html", method = RequestMethod.GET)
    public String printStarted(Model model)
            throws ServletException, IOException, SQLException, ClassNotFoundException, ParseException {
        model.addAttribute("started_races", raceService.readStartedRaces());
        return "startedraces";
    }

    @RequestMapping(value = "/pastrace.html", method = RequestMethod.GET)
    public String printPast(Model model, @RequestParam("name") String name)
            throws ServletException, IOException, SQLException, ClassNotFoundException, ParseException {
        Race race = raceService.findRaceById(raceService.findIdByName(name));
        model.addAttribute("linked_race", race);
        model.addAttribute("results", playersRacesService.readResultsOfRace(race));
        model.addAttribute("players", playersRacesService.readPlayersOnRace(race));
        return "pastrace";
    }

    @RequestMapping(value = "/currentrace.html", method = RequestMethod.GET)
    public String printCurrent(Model model, @RequestParam("name") String name)
            throws ServletException, IOException, SQLException, ClassNotFoundException, ParseException {
        Race race = raceService.findRaceById(raceService.findIdByName(name));
        model.addAttribute("linked_race", race);
        model.addAttribute("players_in_race", playersRacesService.readPlayersOnRace(
                raceService.findRaceById(raceService.findIdByName(name))));
        return "currentrace";
    }

    @RequestMapping(value = "/approvedraces.html", method = RequestMethod.GET)
    public String printApproved(Model model)
            throws ServletException, IOException, SQLException, ClassNotFoundException, ParseException {
        model.addAttribute("races", raceService.readApprovedRaces());
        return "approvedraces";
    }

    @RequestMapping(value = "/futureraces.html", method = RequestMethod.GET)
    public String printFuture(Model model)
            throws ServletException, IOException, SQLException, ClassNotFoundException, ParseException {
        model.addAttribute("future_races", raceService.readCurrentRaces());
        return "futureraces";
    }

    @RequestMapping(value = "/all.html", method = RequestMethod.GET)
    public String printAll(Model model)
            throws ServletException, IOException, SQLException, ClassNotFoundException, ParseException {
        model.addAttribute("all_races", raceService.readAllRaces());
        return "races";
    }

    @RequestMapping(value = "/edit.html", method = RequestMethod.GET)
    public String edit(Model model, @RequestParam("id") Long id)
            throws ServletException, IOException, SQLException, ClassNotFoundException, ParseException {
        model.addAttribute("alltracks", trackService.readAllTracks());
        if (id == -1) {
            Race race = new Race();
            model.addAttribute("raceForm", race);
            return "editrace";
        } else {
            Race race = raceService.findRaceById(id);
            model.addAttribute("linked_race", race);
            model.addAttribute("raceForm", race);
            return "editrace";
        }
    }

    @RequestMapping(value = "/edit.html", method = RequestMethod.POST)
    public String change(@RequestParam("id") Long id,
                         @Valid @ModelAttribute("raceForm") Race race, BindingResult result,
                         @RequestParam("selected_track") String track, Model model)
            throws ServletException, IOException, SQLException, ClassNotFoundException, ParseException, InvocationTargetException {
        if (result.hasErrors()) {
            model.addAttribute("alltracks", trackService.readAllTracks());
            return "editrace";
        } else {
            Long track_id = trackService.findIdByName(track);
            if (id == -1) {
                if (!raceService.existName(race.getName())) {
                    raceService.addRace(race, track_id);
                    return "redirect:/races/all.html?successAdded=true";
                } else {
                    return "redirect:/races/edit.html?id=-1&successAdded=false";
                }
            } else {
                if (!raceService.existName(race.getName()) ||
                        Objects.equals(raceService.findRaceById(id).getName(), race.getName())) {
                    raceService.editRace(id, race, track_id);
                    return "redirect:/races/all.html?successEdited=true";
                } else {
                    return "redirect:/races/edit.html?id=" + id + "&successEdited=false";
                }
            }
        }
    }

    @RequestMapping(value = "/delete.html", method = RequestMethod.POST)
    public String delete(@RequestParam("checked_races") String[] cr)
            throws ServletException, IOException, SQLException, ClassNotFoundException, ParseException {
        try {
            for (String aCr : cr) {
                raceService.deleteRace(Long.parseLong(aCr));
            }
            return "redirect:/races/all.html?successDeleted=true";
        } catch (NullPointerException e) {
            return "redirect:/races/all.html?successDeleted=false";
        }
    }

}
