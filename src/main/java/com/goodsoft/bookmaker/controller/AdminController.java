package com.goodsoft.bookmaker.controller;

import com.goodsoft.bookmaker.entity.contora.Player;
import com.goodsoft.bookmaker.entity.contora.PlayersRaces;
import com.goodsoft.bookmaker.entity.contora.Race;
import com.goodsoft.bookmaker.entity.contora.Rate;
import com.goodsoft.bookmaker.service.account.client.ClientService;
import com.goodsoft.bookmaker.service.contora.clientsrates.ClientsRatesService;
import com.goodsoft.bookmaker.service.contora.player.PlayerService;
import com.goodsoft.bookmaker.service.contora.playersraces.PlayersRacesService;
import com.goodsoft.bookmaker.service.contora.race.RaceService;
import com.goodsoft.bookmaker.service.contora.rate.RateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator.
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private RaceService raceService;
    @Autowired
    private PlayerService playerService;
    @Autowired
    private PlayersRacesService playersRacesService;
    @Autowired
    private RateService rateService;
    @Autowired
    private ClientsRatesService clientsRatesService;
    @Autowired
    private ClientService clientService;

    @RequestMapping(value = "/composition.html", method = RequestMethod.GET)
    public String edit(Model model) throws SQLException, ClassNotFoundException {
        model.addAttribute("all_players", playerService.readAllPlayers());
        return "composition";
    }

    @RequestMapping(value = "/composition.html", method = RequestMethod.POST)
    public String change(@RequestParam("name") String name,
                         @RequestParam("check_players") String[] cp) throws SQLException, ClassNotFoundException, ParseException {
        try {
            Race race = raceService.findRaceById(raceService.findIdByName(name));
            playersRacesService.deleteAllPlayersFromRace(race);
            for (String aCp : cp) {
                playersRacesService.addPlayerToRace(playerService.findPlayerById(playerService.findIdByJokey(aCp)), race);
            }
            raceService.approveRace(raceService.findIdByName(race.getName()));
            return "redirect:/races/futureraces.html";
        } catch (NullPointerException e) {
            return "redirect:/admin/composition.html";
        }
    }

    @RequestMapping(value = "/results.html", method = RequestMethod.GET)
    public String results(Model model, @RequestParam("race") String name) throws SQLException, ClassNotFoundException, ParseException {
        model.addAttribute("players_in_race", playersRacesService
                .readPlayersOnRace(raceService.findRaceById(raceService.findIdByName(name))));
        return "results";
    }

    @RequestMapping(value = "/results.html", method = RequestMethod.POST)
    public String sendMoney(@RequestParam Map<String,String> params) throws SQLException, ClassNotFoundException, ParseException {
        List<Player> players = playersRacesService.
                readPlayersOnRace(raceService.findRaceById(raceService.findIdByName(params.get("race"))));
        Race race = raceService.findRaceById(raceService.findIdByName(params.get("race")));
        List<Rate> rates;
        List<Long> clients;
        for (Player player : players) {
            playersRacesService.setPlayerToPlace(
                    new PlayersRaces(player, race, Integer.parseInt(params.get(player.getJokey()))));  //player.getJokey() - имя списка
            if (rateService.existPlayer(player)) {
                try {
                    rates = rateService.readRatesByPlayer(player);
                    System.out.println(rates.size());
                    for (Rate rate : rates) {
                        System.out.println(rate.getRace().getName() + rate.getPlayer().getJokey() + rate.getRateType());
                        if (clientsRatesService.existRate(rate)) {
                            clients = clientsRatesService.findClientsIdsByRate(rate);
                            for (long client : clients) {
                                clientService.upBalance(clientService.findClientById(client),
                                        rate.getCoefficient()
                                                * clientsRatesService.findClientRate(rate, clientService.findClientById(client)).getAmount());
                            }
                        }
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }
        raceService.closeRace(raceService.findIdByName(params.get("race")));
        return "redirect:/main.html";
    }
}
