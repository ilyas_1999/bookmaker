package com.goodsoft.bookmaker.controller;

import com.goodsoft.bookmaker.entity.contora.Track;
import com.goodsoft.bookmaker.service.contora.track.TrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.sql.SQLException;
import java.util.Objects;

/**
 * Created by Administrator.
 */
@Controller
@RequestMapping("/tracks")
public class TrackController {

    @Autowired
    private TrackService trackService;

    @RequestMapping(value = "/all.html", method = RequestMethod.GET)
    public String printAll(Model model) throws SQLException, ClassNotFoundException {
        model.addAttribute("all_tracks", trackService.readAllTracks());
        return "tracks";
    }

    @RequestMapping(value = "/edit.html", method = RequestMethod.GET)
    public String edit(Model model, @RequestParam("id") Long id) throws SQLException, ClassNotFoundException {
        if (id == -1) {
            Track track = new Track();
            model.addAttribute("trackForm", track);
            return "edittrack";
        } else {
            Track track = trackService.findTrackById(id);
            model.addAttribute("linked_track", track);
            model.addAttribute("trackForm", track);
            return "edittrack";
        }
    }

    @RequestMapping(value = "/edit.html", method = RequestMethod.POST)
    public String change(@RequestParam("id") Long id,
                         @Valid @ModelAttribute("trackForm") Track track, BindingResult result)
            throws SQLException, ClassNotFoundException {
        if (result.hasErrors()) {
            return "edittrack";
        } else {
            if (id == -1) {
                if (!trackService.existName(track.getName())) {
                    trackService.addTrack(track);
                    return "redirect:/tracks/all.html?successAdded=true";
                } else {
                    return "redirect:/tracks/edit.html?id=-1&successAdded=false";
                }
            } else {
                if (!trackService.existName(track.getName()) ||
                        Objects.equals(trackService.findTrackById(id).getName(), track.getName())) {
                    trackService.editTrack(id, track);
                    return "redirect:/tracks/all.html?successEdited=true";
                } else {
                    return "redirect:/tracks/edit.html?id=" + id + "&successEdited=false";
                }
            }
        }
    }

    @RequestMapping(value = "/delete.html", method = RequestMethod.POST)
    public String delete(@RequestParam(value = "checked_tracks", required = false) Long[] ct) throws SQLException, ClassNotFoundException {
        try {
            for (Long aCt : ct) {
                trackService.deleteTrack(aCt);
            }
            return "redirect:/tracks/all.html?successDeleted=true";
        } catch (NullPointerException e) {
            return "redirect:/tracks/all.html?successDeleted=false";
        }
    }
}
