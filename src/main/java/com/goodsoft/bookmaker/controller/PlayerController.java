package com.goodsoft.bookmaker.controller;

import com.goodsoft.bookmaker.entity.contora.Player;
import com.goodsoft.bookmaker.service.contora.player.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.sql.SQLException;
import java.util.Objects;

/**
 * Created by Administrator.
 */
@Controller
@RequestMapping("/players")
public class PlayerController {

    @Autowired
    private PlayerService playerService;

    @RequestMapping(value = "/all.html", method = RequestMethod.GET)
    public String printAll(Model model) throws SQLException, ClassNotFoundException {
        model.addAttribute("all_players", playerService.readAllPlayers());
        return "players";
    }

    @RequestMapping(value = "/edit.html", method = RequestMethod.GET)
    public String edit(Model model, @RequestParam("j") String jokey) throws SQLException, ClassNotFoundException {
        if (jokey.equals("-1")) {
            Player player = new Player();
            model.addAttribute("playerForm",player);
            return "editplayer";
        } else {
            Player player = playerService.findPlayerById(playerService.findIdByJokey(jokey));
            model.addAttribute("linked_player", player);
            model.addAttribute("playerForm",player);
            return "editplayer";
        }
    }

    @RequestMapping(value = "/edit.html", method = RequestMethod.POST)
    public String change(@Valid @ModelAttribute("playerForm") Player player, BindingResult result, @RequestParam("j") String jokey)
            throws SQLException, ClassNotFoundException {
        if(result.hasErrors()){
            return "editplayer";
        }
        else {
            if (jokey.equals("-1")) {
                if (!playerService.existJokey(player.getJokey())) {
                    playerService.addPlayer(player);
                    return "redirect:/players/all.html?successAdded=true";
                } else {
                    return "redirect:/players/edit.html?id=-1&successAdded=false";
                }
            } else {
                if (!playerService.existJokey(player.getJokey()) ||
                        Objects.equals(jokey, player.getJokey())) {
                    playerService.editPlayer(playerService.findIdByJokey(jokey), player);
                    return "redirect:/players/all.html?successEdited=true";
                } else {
                    return "redirect:/players/edit.html?jokey=" + jokey + "&successEdited=false";
                }
            }
        }
    }

    @RequestMapping(value = "/delete.html", method = RequestMethod.POST)
    public String delete(@RequestParam(value = "checked_players", required = false) String[] cp) throws SQLException, ClassNotFoundException {
        try {
            for (String aCu : cp) {
                playerService.deletePlayer(playerService.findIdByJokey(aCu));
            }
            return "redirect:/players/all.html?successDeleted=true";
        } catch (NullPointerException e) {
            return "redirect:/players/all.html?successDeleted=false";
        }
    }
}
