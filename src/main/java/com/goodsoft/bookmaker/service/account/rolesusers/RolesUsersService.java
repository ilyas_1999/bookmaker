package com.goodsoft.bookmaker.service.account.rolesusers;

import com.goodsoft.bookmaker.entity.account.Role;
import com.goodsoft.bookmaker.entity.account.User;

import java.sql.SQLException;

/**
 * Created by Administrator.
 */
public interface RolesUsersService {

    void addUserToRole(User user, Role role) throws SQLException, ClassNotFoundException;

    String findNameByUserId(Long id) throws SQLException, ClassNotFoundException;

    void deleteUserFromRole(Long id) throws SQLException, ClassNotFoundException;
}
