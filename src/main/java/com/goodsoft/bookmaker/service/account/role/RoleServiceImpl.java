package com.goodsoft.bookmaker.service.account.role;

import com.goodsoft.bookmaker.repository.account.role.RoleDAO;
import com.goodsoft.bookmaker.entity.account.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.Map;

/**
 * Created by Administrator.
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDAO roleDAO;

    public Map<Long, Role> readAllRoles() throws SQLException, ClassNotFoundException {
        return roleDAO.readAllRoles();
    }

    public Role findRoleById(Long id) throws SQLException, ClassNotFoundException {
        return roleDAO.findRoleById(id);
    }

    public Long findIdByName(String name) throws SQLException, ClassNotFoundException {
        return roleDAO.findIdByName(name);
    }
}
