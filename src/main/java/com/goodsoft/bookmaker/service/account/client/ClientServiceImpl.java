package com.goodsoft.bookmaker.service.account.client;

import com.goodsoft.bookmaker.repository.account.client.ClientDao;
import com.goodsoft.bookmaker.entity.account.Client;
import com.goodsoft.bookmaker.entity.account.User;
import com.goodsoft.bookmaker.storage.JdbcConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

/**
 * Created by Administrator.
 */
@Service
public class ClientServiceImpl implements ClientService {
    @Autowired
    private ClientDao clientDao;

    public Map<Long, Client> readAllClients() throws SQLException, ClassNotFoundException {
        return clientDao.readAllClients();
    }

    public Client findClientById(Long id) throws SQLException, ClassNotFoundException {
        return clientDao.findClientById(id);
    }

    public Client findClientByUserId(Long id) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.client WHERE user_id=?");
        st.setLong(1, id);
        ResultSet rs = st.executeQuery();
        Client client = new Client();
        while (rs.next()) {
            client.setFirstName(rs.getString(2));
            client.setSecondName(rs.getString(3));
            client.setPassport(rs.getString(4));
            client.setBalance(rs.getDouble(6));
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return client;
    }

    public Long findIdByPassport(String passport) throws SQLException, ClassNotFoundException {
        return clientDao.findIdByPassport(passport);
    }

    public void addClient(Client client, User user) throws SQLException, ClassNotFoundException {
        clientDao.addClient(client, user);
    }

    public void upBalance(Client client, Double summ) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("UPDATE bookmaker.client SET \"balance\"=\"balance\"+? WHERE id=?");
        st.setDouble(1, summ);
        st.setLong(2, clientDao.findIdByPassport(client.getPassport()));
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }

    public void editClient(Long id, Client client) throws SQLException, ClassNotFoundException {
        clientDao.editClient(id, client);
    }


}
