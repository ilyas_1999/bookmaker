package com.goodsoft.bookmaker.service.account.user;

import com.goodsoft.bookmaker.repository.account.user.UserDAO;
import com.goodsoft.bookmaker.entity.account.User;
import com.goodsoft.bookmaker.storage.JdbcConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator.
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDAO userDAO;

    public List<User> readAllUsers() throws SQLException, ClassNotFoundException {
        return userDAO.readAllUsers();
    }

    public User findUserById(Long id) throws SQLException, ClassNotFoundException {
        return userDAO.findUserById(id);
    }

    public Long findIdByLogin(String login) throws SQLException, ClassNotFoundException {
        return userDAO.findIdByLogin(login);
    }

    public List<String> readAllLogins() throws SQLException, ClassNotFoundException {
        return userDAO.readAllLogins();
    }

    public boolean existLogin(String login) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.user WHERE \"login\"=?");
        st.setString(1, login);
        ResultSet rs = st.executeQuery();
        boolean b = rs.isBeforeFirst();
        st.close();
        rs.close();
        jdbcConnector.getConnection().close();
        return b;
    }

    public void addUser(User user) throws SQLException, ClassNotFoundException {
        userDAO.addUser(user);
    }

    public void editUser(Long id, User user) throws SQLException, ClassNotFoundException {
        userDAO.editUser(id, user);
    }

    public void deleteUser(User user) throws SQLException, ClassNotFoundException {
        userDAO.deleteUser(user);
    }
}
