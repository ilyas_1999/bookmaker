package com.goodsoft.bookmaker.service.account.rolesusers;

import com.goodsoft.bookmaker.repository.account.rolesusers.RolesUsersDAO;
import com.goodsoft.bookmaker.entity.account.Role;
import com.goodsoft.bookmaker.entity.account.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;

/**
 * Created by Administrator.
 */
@Service
public class RolesUsersServiceImpl implements RolesUsersService {

    @Autowired
    private RolesUsersDAO rolesUsersDAO;

    public void addUserToRole(User user, Role role) throws SQLException, ClassNotFoundException {
        rolesUsersDAO.addUserToRole(user, role);
    }

    public String findNameByUserId(Long id) throws SQLException, ClassNotFoundException {
       return rolesUsersDAO.findNameByUserId(id);
    }

    public void deleteUserFromRole(Long id) throws SQLException, ClassNotFoundException {
        rolesUsersDAO.deleteUserFromRole(id);
    }

}
