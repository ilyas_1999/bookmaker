package com.goodsoft.bookmaker.service.account.client;

import com.goodsoft.bookmaker.entity.account.Client;
import com.goodsoft.bookmaker.entity.account.User;

import java.sql.SQLException;
import java.util.Map;

/**
 * Created by Administrator.
 */
public interface ClientService {

    Map<Long, Client> readAllClients() throws SQLException, ClassNotFoundException;

    Client findClientById(Long id) throws SQLException, ClassNotFoundException;

    Client findClientByUserId(Long id) throws SQLException, ClassNotFoundException;

    Long findIdByPassport(String passport) throws SQLException, ClassNotFoundException;

    void addClient(Client client, User user) throws SQLException, ClassNotFoundException;

    void upBalance(Client client, Double summ) throws SQLException, ClassNotFoundException;

    void editClient(Long id, Client client) throws SQLException, ClassNotFoundException;


}
