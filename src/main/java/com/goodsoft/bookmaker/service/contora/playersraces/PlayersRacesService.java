package com.goodsoft.bookmaker.service.contora.playersraces;

import com.goodsoft.bookmaker.entity.contora.Player;
import com.goodsoft.bookmaker.entity.contora.PlayersRaces;
import com.goodsoft.bookmaker.entity.contora.Race;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator.
 */
public interface PlayersRacesService {

    Map<Long, PlayersRaces> readAllPlayersRaces() throws SQLException, ClassNotFoundException, ParseException;

    void deletePlayerFromRace(Player player, Race race) throws SQLException, ClassNotFoundException;

    void addPlayerToRace(Player player, Race race) throws SQLException, ClassNotFoundException;

    List<Player> readPlayersOnRace(Race race) throws SQLException, ClassNotFoundException;

    Map<Long, Race> readRacesWithPlayer(Player player) throws SQLException, ClassNotFoundException, ParseException;

    void setPlayerToPlace(PlayersRaces playersRaces) throws SQLException, ClassNotFoundException, ParseException;

    void deleteAllPlayersFromRace(Race race) throws SQLException, ClassNotFoundException;

    List<PlayersRaces> readResultsOfRace(Race race) throws SQLException, ClassNotFoundException, ParseException;
}
