package com.goodsoft.bookmaker.service.contora.playersraces;

import com.goodsoft.bookmaker.repository.contora.player.PlayerDao;
import com.goodsoft.bookmaker.repository.contora.playersraces.PlayersRacesDAO;
import com.goodsoft.bookmaker.repository.contora.race.RaceDao;
import com.goodsoft.bookmaker.entity.contora.Player;
import com.goodsoft.bookmaker.entity.contora.PlayersRaces;
import com.goodsoft.bookmaker.entity.contora.Race;
import com.goodsoft.bookmaker.storage.JdbcConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator.
 */
@Service
public class PlayersRacesServiceImpl implements PlayersRacesService {

    @Autowired
    private PlayersRacesDAO playersRacesDAO;
    @Autowired
    private PlayerDao playerDao;
    @Autowired
    private RaceDao raceDao;

    public Map<Long, PlayersRaces> readAllPlayersRaces() throws SQLException, ClassNotFoundException, ParseException {
        return playersRacesDAO.readAllPlayersRaces();
    }

    public void deletePlayerFromRace(Player player, Race race) throws SQLException, ClassNotFoundException {
        playersRacesDAO.deletePlayerFromRace(player, race);
    }

    public void addPlayerToRace(Player player, Race race) throws SQLException, ClassNotFoundException {
        playersRacesDAO.addPlayerToRace(player, race);
    }

    public List<Player> readPlayersOnRace(Race race) throws SQLException, ClassNotFoundException {
        return playersRacesDAO.readPlayersOnRace(race);
    }

    public Map<Long, Race> readRacesWithPlayer(Player player) throws SQLException, ClassNotFoundException, ParseException {
        return playersRacesDAO.readRacesWithPlayer(player);
    }

    public void setPlayerToPlace(PlayersRaces playersRaces) throws SQLException, ClassNotFoundException, ParseException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("UPDATE bookmaker.players_races SET \"place\"=? WHERE \"player_id\"=? AND \"race_id\"=?");
        st.setInt(1,playersRaces.getPlace());
        st.setLong(2, playerDao.findIdByJokey(playersRaces.getPlayer().getJokey()));
        st.setLong(3, raceDao.findIdByName(playersRaces.getRace().getName()));
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }

    public void deleteAllPlayersFromRace(Race race) throws SQLException, ClassNotFoundException {
        playersRacesDAO.deleteAllPlayersFromRace(race);
    }

    public List<PlayersRaces> readResultsOfRace(Race race) throws SQLException, ClassNotFoundException, ParseException {
        return playersRacesDAO.readResultsOfRace(race);
    }
}
