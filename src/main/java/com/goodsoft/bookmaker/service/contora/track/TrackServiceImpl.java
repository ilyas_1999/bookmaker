package com.goodsoft.bookmaker.service.contora.track;

import com.goodsoft.bookmaker.repository.contora.track.TrackDAO;
import com.goodsoft.bookmaker.entity.contora.Track;
import com.goodsoft.bookmaker.storage.JdbcConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

/**
 * Created by Administrator.
 */
@Service
public class TrackServiceImpl implements TrackService {

    @Autowired
    private TrackDAO trackDAO;

    public Map<Long, Track> readAllTracks() throws SQLException, ClassNotFoundException {
        return trackDAO.readAllTracks();
    }

    public Track findTrackById(Long id) throws SQLException, ClassNotFoundException {
        return trackDAO.findTrackById(id);
    }

    public Long findIdByName(String name) throws SQLException, ClassNotFoundException {
        return trackDAO.findIdByName(name);
    }

    public void addTrack(Track track) throws SQLException, ClassNotFoundException {
        trackDAO.addTrack(track);
    }

    public void editTrack(Long id, Track track) throws SQLException, ClassNotFoundException {
        trackDAO.editTrack(id, track);
    }

    public void deleteTrack(Long id) throws SQLException, ClassNotFoundException {
        trackDAO.deleteTrack(id);
    }

    public boolean existName(String name) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.track WHERE \"name\"=?");
        st.setString(1, name);
        ResultSet rs = st.executeQuery();
        boolean b = rs.isBeforeFirst();
        st.close();
        rs.close();
        jdbcConnector.getConnection().close();
        return b;
    }
}
