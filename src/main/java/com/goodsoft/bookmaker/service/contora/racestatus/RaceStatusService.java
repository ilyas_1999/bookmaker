package com.goodsoft.bookmaker.service.contora.racestatus;

import com.goodsoft.bookmaker.entity.contora.RaceStatus;

import java.sql.SQLException;

/**
 * Created by Administrator.
 */
public interface RaceStatusService {

    RaceStatus findRaceStatusByRaceId(Long id) throws SQLException, ClassNotFoundException;
}
