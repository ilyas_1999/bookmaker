package com.goodsoft.bookmaker.service.contora.race;

import com.goodsoft.bookmaker.repository.contora.race.RaceDao;
import com.goodsoft.bookmaker.entity.contora.Race;
import com.goodsoft.bookmaker.repository.contora.race.RaceDaoImpl;
import com.goodsoft.bookmaker.storage.JdbcConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class RaceServiceImpl implements RaceService {

    @Autowired
    private RaceDao raceDao;

    public Map<Long, Race> readAllRaces() throws SQLException, ClassNotFoundException, ParseException {
        return raceDao.readAllRaces();
    }

    public Map<Long, Race> readCurrentRaces() throws SQLException, ClassNotFoundException, ParseException {
        return raceDao.readCurrentRaces();
    }

    public Map<Long, Race> readPastRaces() throws SQLException, ClassNotFoundException, ParseException {
        return raceDao.readPastRaces();
    }

    public Map<Long, Race> readTodayRaces() throws SQLException, ClassNotFoundException, ParseException {
        return raceDao.readTodayRaces();
    }

    public List<Race> readStartedRaces() throws SQLException, ClassNotFoundException, ParseException {
        return raceDao.readStartedRaces();
    }

    public List<Race> readApprovedRaces() throws SQLException, ClassNotFoundException, ParseException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.race WHERE \"status_id\"=3 AND \"date\" >= now()::DATE ORDER BY \"date\" DESC");
        ResultSet rs = st.executeQuery();
        List<Race> races = new ArrayList<>();
        Race race;
        while (rs.next()) {
            race = new Race();
            race.setDate(rs.getString(2));
            race.setTime(rs.getString(6));
            race.setName(rs.getString(5));
            races.add(race);
        }
        st.close();
        rs.close();
        jdbcConnector.getConnection().close();
        return races;
    }

    public List<Race> readClosedRaces() throws SQLException, ClassNotFoundException, ParseException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.race WHERE \"status_id\"=2 ORDER BY \"date\" DESC");
        ResultSet rs = st.executeQuery();
        List<Race> races = new ArrayList<>();
        Race race;
        while (rs.next()) {
            race = new Race();
            race.setDate(rs.getString(2));
            race.setTime(rs.getString(6));
            race.setName(rs.getString(5));
            races.add(race);
        }
        st.close();
        rs.close();
        jdbcConnector.getConnection().close();
        return races;
    }

    public Race findRaceById(Long id) throws SQLException, ClassNotFoundException, ParseException {
        return raceDao.findRaceById(id);
    }

    public void deleteRace(Long id) throws SQLException, ClassNotFoundException {
        raceDao.deleteRace(id);
    }

    public void addRace(Race race, Long track_id) throws SQLException, ClassNotFoundException, ParseException, InvocationTargetException {
        raceDao.addRace(race, track_id);
    }

    public void editRace(Long id, Race race, Long track_id) throws SQLException, ClassNotFoundException, ParseException, InvocationTargetException {
        raceDao.editRace(id, race, track_id);
    }

    public void approveRace(Long id) throws SQLException, ClassNotFoundException, ParseException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("UPDATE bookmaker.race SET \"status_id\"=3 WHERE id=?");
        st.setLong(1, id);
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }

    public void closeRace(Long id) throws SQLException, ClassNotFoundException, ParseException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("UPDATE bookmaker.race SET \"status_id\"=2 WHERE id=?");
        st.setLong(1, id);
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }

    public Long findIdByName(String name) throws SQLException, ClassNotFoundException {
        return raceDao.findIdByName(name);
    }

    public boolean existName(String name) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.race WHERE \"name\"=?");
        st.setString(1, name);
        ResultSet rs = st.executeQuery();
        boolean b = rs.isBeforeFirst();
        st.close();
        rs.close();
        jdbcConnector.getConnection().close();
        return b;
    }
}
