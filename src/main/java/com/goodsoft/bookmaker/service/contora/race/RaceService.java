package com.goodsoft.bookmaker.service.contora.race;

import com.goodsoft.bookmaker.entity.contora.Race;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator.
 */
public interface RaceService {

    Map<Long, Race> readAllRaces() throws SQLException, ClassNotFoundException, ParseException;

    Map<Long, Race> readCurrentRaces() throws SQLException, ClassNotFoundException, ParseException;

    Map<Long, Race> readPastRaces() throws SQLException, ClassNotFoundException, ParseException;

    Map<Long, Race> readTodayRaces() throws SQLException, ClassNotFoundException, ParseException;

    List<Race> readStartedRaces() throws SQLException, ClassNotFoundException, ParseException;

    List<Race> readApprovedRaces() throws SQLException, ClassNotFoundException, ParseException;

    List<Race> readClosedRaces() throws SQLException, ClassNotFoundException, ParseException;

    Race findRaceById(Long id) throws SQLException, ClassNotFoundException, ParseException;

    void deleteRace(Long id) throws SQLException, ClassNotFoundException;

    void addRace(Race race, Long track_id) throws SQLException, ClassNotFoundException, ParseException, InvocationTargetException;

    void editRace(Long id, Race race, Long track_id) throws SQLException, ClassNotFoundException, ParseException, InvocationTargetException;

    void approveRace(Long id) throws SQLException, ClassNotFoundException, ParseException;

    void closeRace(Long id) throws SQLException, ClassNotFoundException, ParseException;

    Long findIdByName(String name) throws SQLException, ClassNotFoundException;

    boolean existName(String name) throws SQLException, ClassNotFoundException;
}
