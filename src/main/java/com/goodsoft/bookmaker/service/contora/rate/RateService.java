package com.goodsoft.bookmaker.service.contora.rate;

import com.goodsoft.bookmaker.entity.contora.Player;
import com.goodsoft.bookmaker.entity.contora.Race;
import com.goodsoft.bookmaker.entity.contora.Rate;
import com.goodsoft.bookmaker.entity.contora.RateType;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator.
 */
public interface RateService {

    Map<Long, Rate> readAllRates() throws SQLException, ClassNotFoundException, ParseException;

    List<Rate> readRatesByPlayer(Player player) throws SQLException, ClassNotFoundException, ParseException;

    Rate findRateById(Long id) throws SQLException, ClassNotFoundException, ParseException;

    Long findIdByAll(Long playerId, Long raceId, Long rateTypeId) throws SQLException, ClassNotFoundException;

    void deleteRate(Long id) throws SQLException, ClassNotFoundException;

    void editRate(Long id, Rate rate, Race race, RateType rateType, Player player)
            throws SQLException, ClassNotFoundException;

    void addRate(Rate rate) throws SQLException, ClassNotFoundException;

    boolean existPlayer(Player player) throws SQLException, ClassNotFoundException;

    boolean existRate(Rate rate) throws SQLException, ClassNotFoundException;

    Long findRateTypeIdById(Long rate_id) throws SQLException, ClassNotFoundException;

}
