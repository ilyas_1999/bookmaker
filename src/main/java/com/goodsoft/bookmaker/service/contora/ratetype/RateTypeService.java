package com.goodsoft.bookmaker.service.contora.ratetype;

import com.goodsoft.bookmaker.entity.contora.Player;
import com.goodsoft.bookmaker.entity.contora.Race;
import com.goodsoft.bookmaker.entity.contora.RateType;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator.
 */
public interface RateTypeService {

    Map<Long, RateType> readAllRateTypes() throws SQLException, ClassNotFoundException;

    RateType findRateTypeById(Long id) throws SQLException, ClassNotFoundException;

    Long findIdByName(String name) throws SQLException, ClassNotFoundException;

    List<RateType> readRateTypesByRaceAndPlayer(Race race, Player player) throws SQLException, ClassNotFoundException;
}
