package com.goodsoft.bookmaker.service.contora.racestatus;

import com.goodsoft.bookmaker.repository.contora.racestatus.RaceStatusDAO;
import com.goodsoft.bookmaker.entity.contora.RaceStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;

/**
 * Created by Administrator.
 */
@Service
public class RaceStatusServiceImpl implements RaceStatusService {

    @Autowired
    private RaceStatusDAO raceStatusDAO;

    public RaceStatus findRaceStatusByRaceId(Long id) throws SQLException, ClassNotFoundException {
        return raceStatusDAO.findRaceStatusByRaceId(id);
    }
}
