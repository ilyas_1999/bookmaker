package com.goodsoft.bookmaker.service.contora.clientsrates;

import com.goodsoft.bookmaker.repository.account.client.ClientDao;
import com.goodsoft.bookmaker.repository.contora.clientsrates.ClientsRatesDAO;
import com.goodsoft.bookmaker.repository.contora.player.PlayerDao;
import com.goodsoft.bookmaker.repository.contora.race.RaceDao;
import com.goodsoft.bookmaker.repository.contora.rate.RateDao;
import com.goodsoft.bookmaker.repository.contora.ratetype.RateTypeDAO;
import com.goodsoft.bookmaker.entity.account.Client;
import com.goodsoft.bookmaker.entity.contora.ClientsRates;
import com.goodsoft.bookmaker.entity.contora.Rate;
import com.goodsoft.bookmaker.entity.contora.RateType;
import com.goodsoft.bookmaker.service.contora.rate.RateService;
import com.goodsoft.bookmaker.storage.JdbcConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator.
 */
@Service
public class ClientsRatesServiceImpl implements ClientsRatesService {

    @Autowired
    private ClientsRatesDAO clientsRatesDAO;
    @Autowired
    private RateDao rateDao;
    @Autowired
    private PlayerDao playerDao;
    @Autowired
    private RaceDao raceDao;
    @Autowired
    private RateTypeDAO rateTypeDAO;
    @Autowired
    private ClientDao clientDao;
    @Autowired
    private RateService rateService;

    public Map<Long, ClientsRates> readAllClientRates() throws SQLException, ClassNotFoundException, ParseException {
        return clientsRatesDAO.readAllClientRates();
    }

    public void addRateToClient(Client client, Rate rate, Double amount) throws SQLException, ClassNotFoundException {
        clientsRatesDAO.addRateToClient(client, rate, amount);
    }

    public Long findCountOfRates(Rate rate) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT COUNT(*) FROM bookmaker.clients_rates WHERE \"rate_id\"=?");
        st.setLong(1, rateDao.findIdByAll(playerDao.findIdByJokey(rate.getPlayer().getJokey()),
                raceDao.findIdByName(rate.getRace().getName()), rateTypeDAO.findIdByName(rate.getRateType().getName())));
        ResultSet rs = st.executeQuery();
        Long count = null;
        while (rs.next()) {
            count = rs.getLong(1);
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return count;
    }

    public boolean existRate(Client client, Rate rate, Double amount) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.clients_rates " +
                        "WHERE client_id=? AND rate_id =?");
        st.setLong(1, clientDao.findIdByPassport(client.getPassport()));
        st.setLong(2, rateDao.findIdByAll(playerDao.findIdByJokey(rate.getPlayer().getJokey()),
                raceDao.findIdByName(rate.getRace().getName()), rateTypeDAO.findIdByName(rate.getRateType().getName())));
        ResultSet rs = st.executeQuery();
        boolean b = rs.isBeforeFirst();
        if (b) {
            st = jdbcConnector.getConnection()
                    .prepareStatement("UPDATE bookmaker.clients_rates SET \"amount\" = \"amount\"+? " +
                            "WHERE client_id=? AND rate_id =?");
            st.setDouble(1, amount);
            st.setLong(2, clientDao.findIdByPassport(client.getPassport()));
            st.setLong(3, rateDao.findIdByAll(playerDao.findIdByJokey(rate.getPlayer().getJokey()),
                    raceDao.findIdByName(rate.getRace().getName()), rateTypeDAO.findIdByName(rate.getRateType().getName())));
            st.executeUpdate();

            st = jdbcConnector.getConnection()
                    .prepareStatement("UPDATE bookmaker.client SET \"balance\" = \"balance\"-? WHERE id=?");
            st.setDouble(1, amount);
            st.setLong(2, clientDao.findIdByPassport(client.getPassport()));
            st.executeUpdate();
        }
        st.close();
        rs.close();
        jdbcConnector.getConnection().close();
        return b;
    }

    public boolean existRate(Rate rate) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.clients_rates WHERE rate_id =?");
        st.setLong(1, rateDao.findIdByAll(playerDao.findIdByJokey(rate.getPlayer().getJokey()),
                raceDao.findIdByName(rate.getRace().getName()), rateTypeDAO.findIdByName(rate.getRateType().getName())));
        ResultSet rs = st.executeQuery();
        boolean b = rs.isBeforeFirst();
        st.close();
        rs.close();
        jdbcConnector.getConnection().close();
        return b;
    }

    public Double possibleReward(Rate rate, RateType rateType)
            throws SQLException, ClassNotFoundException, ParseException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT \"coefficient\" FROM bookmaker.rate_parameters WHERE id=?");
        st.setLong(1, rateDao.findIdByAll(playerDao.findIdByJokey(rate.getPlayer().getJokey()),
                raceDao.findIdByName(rate.getRace().getName()), rateTypeDAO.findIdByName(rate.getRateType().getName())));
        ResultSet rs = st.executeQuery();
        Double coeff = null;
        while (rs.next()) {
            coeff = rs.getDouble(1);
        }
        st = jdbcConnector.getConnection()
                .prepareStatement("SELECT COUNT(*) FROM bookmaker.clients_rates cr RIGHT JOIN bookmaker.rate_parameters rp ON cr.rate_id=rp.id\n" +
                        "WHERE rp.rate_type_id=? AND rp.race_id=? AND rp.player_id=?");
        st.setLong(1, rateTypeDAO.findIdByName(rateType.getName()));
        st.setLong(2, raceDao.findIdByName(rateDao.findRateById(rateDao.findIdByAll(
                playerDao.findIdByJokey(rate.getPlayer().getJokey()),
                raceDao.findIdByName(rate.getRace().getName()),
                rateTypeDAO.findIdByName(rate.getRateType().getName()))).getRace().getName()));
        st.setLong(3, playerDao.findIdByJokey(rateDao.findRateById(rateDao.findIdByAll(
                playerDao.findIdByJokey(rate.getPlayer().getJokey()),
                raceDao.findIdByName(rate.getRace().getName()),
                rateTypeDAO.findIdByName(rate.getRateType().getName()))).getPlayer().getJokey()));
        rs = st.executeQuery();
        Double count = null;
        while (rs.next()) {
            count = rs.getDouble(1);
        }
        return count / coeff;
    }

    public List<Long> findClientsIdsByRate(Rate rate) throws SQLException, ClassNotFoundException, ParseException {
        if (rateService.existRate(rate)) {
            JdbcConnector jdbcConnector = new JdbcConnector();
            PreparedStatement st = jdbcConnector.getConnection()
                    .prepareStatement("SELECT \"client_id\" FROM bookmaker.clients_rates WHERE \"rate_id\"=?");
            st.setLong(1, rateDao.findIdByAll(playerDao.findIdByJokey(rate.getPlayer().getJokey()),
                    raceDao.findIdByName(rate.getRace().getName()), rateTypeDAO.findIdByName(rate.getRateType().getName())));
            ResultSet rs = st.executeQuery();
            List<Long> clients = new ArrayList<>();
            while (rs.next()) {
                clients.add(rs.getLong(1));
            }
            return clients;
        } else {
            return null;
        }
    }

    public ClientsRates findClientRate(Rate rate, Client client) throws SQLException, ClassNotFoundException, ParseException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.clients_rates WHERE \"rate_id\"=? AND \"client_id\"=?");
        st.setLong(1, rateDao.findIdByAll(playerDao.findIdByJokey(rate.getPlayer().getJokey()),
                raceDao.findIdByName(rate.getRace().getName()), rateTypeDAO.findIdByName(rate.getRateType().getName())));
        st.setLong(2, clientDao.findIdByPassport(client.getPassport()));
        ResultSet rs = st.executeQuery();
        ClientsRates clientRate = null;
        while (rs.next()) {
            clientRate = new ClientsRates(clientDao.findClientById(rs.getLong(1)), rateDao.findRateById(rs.getLong(2)), rs.getDouble(3));
        }
        return clientRate;
    }

    public List<ClientsRates> findClientRateByClient(Client client) throws SQLException, ClassNotFoundException, ParseException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.clients_rates WHERE \"client_id\"=?");
        st.setLong(1, clientDao.findIdByPassport(client.getPassport()));
        ResultSet rs = st.executeQuery();
        List<ClientsRates> clientsRates = new ArrayList<>();
        while (rs.next()) {
            clientsRates.add(new ClientsRates(clientDao.findClientById(rs.getLong(1)), rateDao.findRateById(rs.getLong(2)), rs.getDouble(3)));
        }
        return clientsRates;
    }
}
