package com.goodsoft.bookmaker.service.contora.ratetype;

import com.goodsoft.bookmaker.repository.contora.player.PlayerDao;
import com.goodsoft.bookmaker.repository.contora.race.RaceDao;
import com.goodsoft.bookmaker.repository.contora.ratetype.RateTypeDAO;
import com.goodsoft.bookmaker.entity.contora.Player;
import com.goodsoft.bookmaker.entity.contora.Race;
import com.goodsoft.bookmaker.entity.contora.RateType;
import com.goodsoft.bookmaker.storage.JdbcConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator.
 */
@Service
public class RateTypeServiceImpl implements RateTypeService {

    @Autowired
    private RateTypeDAO rateTypeDAO;
    @Autowired
    private PlayerDao playerDao;
    @Autowired
    private RaceDao raceDao;

    public Map<Long, RateType> readAllRateTypes() throws SQLException, ClassNotFoundException {
        return rateTypeDAO.readAllRateTypes();
    }

    public RateType findRateTypeById(Long id) throws SQLException, ClassNotFoundException {
        return rateTypeDAO.findRateTypeById(id);
    }

    public Long findIdByName(String name) throws SQLException, ClassNotFoundException {
        return rateTypeDAO.findIdByName(name);
    }

    public List<RateType> readRateTypesByRaceAndPlayer(Race race, Player player) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT name FROM bookmaker.rate_type rt JOIN bookmaker.rate_parameters rp ON rp.rate_type_id=rt.id " +
                        "WHERE rp.race_id=? AND rp.player_id=?");
        st.setLong(1, raceDao.findIdByName(race.getName()));
        st.setLong(2, playerDao.findIdByJokey(player.getJokey()));
        ResultSet rs = st.executeQuery();
        List<RateType> rateTypes = new ArrayList<>();
        while (rs.next()) {
            rateTypes.add(new RateType(rs.getString(1)));
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return rateTypes;
    }
}
