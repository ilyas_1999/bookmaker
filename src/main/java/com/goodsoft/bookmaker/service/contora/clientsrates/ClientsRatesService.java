package com.goodsoft.bookmaker.service.contora.clientsrates;

import com.goodsoft.bookmaker.entity.account.Client;
import com.goodsoft.bookmaker.entity.contora.ClientsRates;
import com.goodsoft.bookmaker.entity.contora.Rate;
import com.goodsoft.bookmaker.entity.contora.RateType;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator.
 */
public interface ClientsRatesService {

    Map<Long, ClientsRates> readAllClientRates() throws SQLException, ClassNotFoundException, ParseException;

    void addRateToClient(Client client, Rate rate, Double amount) throws SQLException, ClassNotFoundException;

    Long findCountOfRates(Rate rate) throws SQLException, ClassNotFoundException;

    boolean existRate(Client client, Rate rate, Double amount) throws SQLException, ClassNotFoundException;

    boolean existRate(Rate rate) throws SQLException, ClassNotFoundException;

    Double possibleReward(Rate rate, RateType rateType) throws SQLException, ClassNotFoundException, ParseException;

    List<Long> findClientsIdsByRate(Rate rate) throws SQLException, ClassNotFoundException, ParseException;

    ClientsRates findClientRate(Rate rate, Client client) throws SQLException, ClassNotFoundException, ParseException;

    List<ClientsRates> findClientRateByClient(Client client) throws SQLException, ClassNotFoundException, ParseException;
}
