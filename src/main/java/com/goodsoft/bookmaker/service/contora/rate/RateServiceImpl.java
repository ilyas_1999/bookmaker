package com.goodsoft.bookmaker.service.contora.rate;

import com.goodsoft.bookmaker.repository.contora.player.PlayerDao;
import com.goodsoft.bookmaker.repository.contora.race.RaceDao;
import com.goodsoft.bookmaker.repository.contora.rate.RateDao;
import com.goodsoft.bookmaker.repository.contora.ratetype.RateTypeDAO;
import com.goodsoft.bookmaker.entity.contora.Player;
import com.goodsoft.bookmaker.entity.contora.Race;
import com.goodsoft.bookmaker.entity.contora.Rate;
import com.goodsoft.bookmaker.entity.contora.RateType;
import com.goodsoft.bookmaker.storage.JdbcConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator.
 */
@Service
public class RateServiceImpl implements RateService {

    @Autowired
    private RateDao rateDao;
    @Autowired
    private PlayerDao playerDao;
    @Autowired
    private RaceDao raceDao;
    @Autowired
    private RateTypeDAO rateTypeDAO;

    public Map<Long, Rate> readAllRates() throws SQLException, ClassNotFoundException, ParseException {
        return rateDao.readAllRates();
    }

    public List<Rate> readRatesByPlayer(Player player) throws SQLException, ClassNotFoundException, ParseException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.rate_parameters WHERE \"player_id\"=?");
        st.setLong(1,playerDao.findIdByJokey(player.getJokey()));
        ResultSet rs = st.executeQuery();
       List<Rate> rates = new ArrayList<>();
        while (rs.next()) {
            rates.add(new Rate(rs.getDouble(8), rs.getInt(2), rs.getDouble(3), rs.getDouble(4),
                    rateTypeDAO.findRateTypeById(rs.getLong(5)), raceDao.findRaceById(rs.getLong(6)), playerDao.findPlayerById(rs.getLong(7))));
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return rates;
    }

    public Rate findRateById(Long id) throws SQLException, ClassNotFoundException, ParseException {
        return rateDao.findRateById(id);
    }

    public Long findIdByAll(Long playerId, Long raceId, Long rateTypeId) throws SQLException, ClassNotFoundException {
        return rateDao.findIdByAll(playerId, raceId, rateTypeId);
    }

    public void deleteRate(Long id) throws SQLException, ClassNotFoundException {
        rateDao.deleteRate(id);
    }

    public void editRate(Long id, Rate rate, Race race, RateType rateType, Player player)
            throws SQLException, ClassNotFoundException {
        rateDao.editRate(id, rate, race, rateType, player);
    }

    public void addRate(Rate rate) throws SQLException, ClassNotFoundException {
        rateDao.addRate(rate);
    }

    public boolean existRate(Rate rate) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.rate_parameters rp " +
                        "WHERE rp.player_id=? AND rp.race_id =? AND rp.rate_type_id=?");
        st.setLong(1, playerDao.findIdByJokey(rate.getPlayer().getJokey()));
        st.setLong(2, raceDao.findIdByName(rate.getRace().getName()));
        st.setLong(3, rateTypeDAO.findIdByName(rate.getRateType().getName()));
        ResultSet rs = st.executeQuery();
        boolean b = rs.isBeforeFirst();
        st.close();
        rs.close();
        jdbcConnector.getConnection().close();
        return b;
    }

    public boolean existPlayer(Player player) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.rate_parameters rp WHERE rp.player_id=?");
        st.setLong(1, playerDao.findIdByJokey(player.getJokey()));
        ResultSet rs = st.executeQuery();
        boolean b = rs.isBeforeFirst();
        st.close();
        rs.close();
        jdbcConnector.getConnection().close();
        return b;
    }

    public Long findRateTypeIdById(Long rate_id) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT \"rate_type_id\" FROM bookmaker.rate_parameters WHERE id=?");
        st.setLong(1, rate_id);
        ResultSet rs = st.executeQuery();
        Long id = null;
        while (rs.next()) {
            id = rs.getLong(1);
        }
        st.close();
        rs.close();
        jdbcConnector.getConnection().close();
        return id;
    }
}
