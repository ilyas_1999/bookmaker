package com.goodsoft.bookmaker.service.contora.player;

import com.goodsoft.bookmaker.entity.contora.Player;
import com.goodsoft.bookmaker.repository.contora.player.PlayerDao;
import com.goodsoft.bookmaker.storage.JdbcConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Administrator.
 */
@Service
public class PlayerServiceImpl implements PlayerService {

    @Autowired
    private PlayerDao playerDao;

    public List<Player> readAllPlayers() throws SQLException, ClassNotFoundException {
        return playerDao.readAllPlayers();
    }

    public Player findPlayerById(Long id) throws SQLException, ClassNotFoundException {
        return playerDao.findPlayerById(id);
    }

    public Long findIdByJokey(String jokey) throws SQLException, ClassNotFoundException {
        return playerDao.findIdByJokey(jokey);
    }

    public void deletePlayer(Long id) throws SQLException, ClassNotFoundException {
        playerDao.deletePlayer(id);
    }

    public void editPlayer(Long id, Player player) throws SQLException, ClassNotFoundException {
        playerDao.editPlayer(id, player);
    }
    public boolean existJokey(String jokey) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.player WHERE \"jokey\"=?");
        st.setString(1, jokey);
        ResultSet rs = st.executeQuery();
        boolean b = rs.isBeforeFirst();
        st.close();
        rs.close();
        jdbcConnector.getConnection().close();
        return b;
    }

    public void addPlayer(Player player) throws SQLException, ClassNotFoundException {
        playerDao.addPlayer(player);
    }
}
