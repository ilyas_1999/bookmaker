package com.goodsoft.bookmaker.repository.contora.rate;

import com.goodsoft.bookmaker.entity.contora.Player;
import com.goodsoft.bookmaker.entity.contora.Race;
import com.goodsoft.bookmaker.entity.contora.Rate;
import com.goodsoft.bookmaker.entity.contora.RateType;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Map;

/**
 * Created by Administrator.
 */
public interface RateDao {

    Map<Long, Rate> readAllRates() throws SQLException, ClassNotFoundException, ParseException;

    Rate findRateById(Long id) throws SQLException, ClassNotFoundException, ParseException;

    Long findIdByAll(Long playerId, Long raceId, Long raceTypeId) throws SQLException, ClassNotFoundException;

    void addRate(Rate rate) throws SQLException, ClassNotFoundException;

    void deleteRate(Long id) throws SQLException, ClassNotFoundException;

    void editRate(Long id, Rate rate, Race race, RateType rateType, Player player) throws SQLException, ClassNotFoundException;
}
