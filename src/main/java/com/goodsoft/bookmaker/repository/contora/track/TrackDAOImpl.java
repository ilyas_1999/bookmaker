package com.goodsoft.bookmaker.repository.contora.track;

import com.goodsoft.bookmaker.repository.contora.race.RaceDao;
import com.goodsoft.bookmaker.entity.contora.Track;
import com.goodsoft.bookmaker.storage.JdbcConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator.
 */
@Repository
public class TrackDAOImpl implements TrackDAO {

    @Autowired
    private RaceDao raceDao;

    public Map<Long, Track> readAllTracks() throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.track ORDER BY \"city\"");
        ResultSet rs = st.executeQuery();
        Map<Long, Track> clients = new HashMap<>();
        Track track;
        while (rs.next()) {
            track = new Track();
            track.setName(rs.getString(2));
            track.setType(rs.getString(3));
            track.setCity(rs.getString(4));
            clients.put(rs.getLong(1), track);
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return clients;
    }

    public Track findTrackById(Long id) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.track WHERE id=?");
        st.setLong(1, id);
        ResultSet rs = st.executeQuery();
        Track track = new Track();
        while (rs.next()) {
            track.setName(rs.getString(2));
            track.setType(rs.getString(3));
            track.setCity(rs.getString(4));
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return track;
    }

    public Long findIdByName(String name) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT id FROM bookmaker.track WHERE \"name\"=?");
        st.setString(1, name);
        ResultSet rs = st.executeQuery();
        Long id = null;
        while (rs.next()) {
            id = rs.getLong(1);
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return id;
    }

    public void addTrack(Track track) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("INSERT INTO bookmaker.track (\"name\", \"type\", \"city\") VALUES (?,?,?);");
        st.setString(1, track.getName());
        st.setString(2, track.getType());
        st.setString(3, track.getCity());
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }

    public void editTrack(Long id, Track track) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("UPDATE bookmaker.track SET \"name\"=?, \"type\"=?, \"city\"=? WHERE id=?");
        st.setString(1, track.getName());
        st.setString(2, track.getType());
        st.setString(3, track.getCity());
        st.setLong(4, id);
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }

    public void deleteTrack(Long id) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.race WHERE track_id=?");
        st.setLong(1, id);
        ResultSet rs = st.executeQuery();
        while (rs.next()) {
            raceDao.deleteRace(rs.getLong(1));
        }
        st = jdbcConnector.getConnection()
                .prepareStatement("DELETE FROM bookmaker.track WHERE id=?");
        st.setLong(1, id);
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }
}
