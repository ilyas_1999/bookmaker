package com.goodsoft.bookmaker.repository.contora.playersraces;

import com.goodsoft.bookmaker.repository.contora.player.PlayerDao;
import com.goodsoft.bookmaker.repository.contora.race.RaceDao;
import com.goodsoft.bookmaker.entity.contora.Player;
import com.goodsoft.bookmaker.entity.contora.PlayersRaces;
import com.goodsoft.bookmaker.entity.contora.Race;
import com.goodsoft.bookmaker.storage.JdbcConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class PlayersRacesDAOImpl implements PlayersRacesDAO {

    @Autowired
    private PlayerDao playerDao;
    @Autowired
    private RaceDao raceDao;

    public Map<Long, PlayersRaces> readAllPlayersRaces() throws SQLException, ClassNotFoundException, ParseException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.players_races");
        ResultSet rs = st.executeQuery();
        Map<Long, PlayersRaces> playersRaces = new LinkedHashMap<>();
        while (rs.next()) {
            playersRaces.put(rs.getLong(1), new PlayersRaces(playerDao.findPlayerById(rs.getLong(1)),
                    raceDao.findRaceById(rs.getLong(2)), rs.getInt(3)));
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return playersRaces;
    }

    public void deletePlayerFromRace(Player player, Race race) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("DELETE FROM bookmaker.players_races WHERE \"player_id\"=? AND \"race_id\"=?");
        st.setLong(1, playerDao.findIdByJokey(player.getJokey()));
        st.setLong(2, raceDao.findIdByName(race.getName()));
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }

    public void deleteAllPlayersFromRace(Race race) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("DELETE FROM bookmaker.players_races WHERE \"race_id\"=?");
        st.setLong(1, raceDao.findIdByName(race.getName()));
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }

    public void addPlayerToRace(Player player, Race race) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("INSERT INTO bookmaker.players_races (\"player_id\", \"race_id\") VALUES (?,?);");
        st.setLong(1, playerDao.findIdByJokey(player.getJokey()));
        st.setLong(2, raceDao.findIdByName(race.getName()));
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }

    public List<Player> readPlayersOnRace(Race race) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.players_races WHERE race_id=?");
        st.setLong(1, raceDao.findIdByName(race.getName()));
        ResultSet rs = st.executeQuery();
        List<Player> playersOnRace = new ArrayList<>();
        while (rs.next()) {
            playersOnRace.add(playerDao.findPlayerById(rs.getLong(1)));
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return playersOnRace;
    }

    public List<PlayersRaces> readResultsOfRace(Race race) throws SQLException, ClassNotFoundException, ParseException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.players_races WHERE race_id=?");
        st.setLong(1, raceDao.findIdByName(race.getName()));
        ResultSet rs = st.executeQuery();
        List<PlayersRaces> playersRaces = new ArrayList<>();
        while (rs.next()) {
            playersRaces.add(new PlayersRaces(
                    playerDao.findPlayerById(rs.getLong(1)), raceDao.findRaceById(rs.getLong(2)), rs.getInt(3)));
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return playersRaces;
    }

    public Map<Long, Race> readRacesWithPlayer(Player player) throws SQLException, ClassNotFoundException, ParseException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.players_races WHERE player_id=?");
        st.setLong(1, playerDao.findIdByJokey(player.getJokey()));
        ResultSet rs = st.executeQuery();
        Map<Long, Race> racesWithPlayer = new LinkedHashMap<>();
        while (rs.next()) {
            racesWithPlayer.put(rs.getLong(1), raceDao.findRaceById(rs.getLong(2)));
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return racesWithPlayer;
    }
}
