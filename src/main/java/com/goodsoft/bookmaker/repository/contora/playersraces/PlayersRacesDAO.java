package com.goodsoft.bookmaker.repository.contora.playersraces;

import com.goodsoft.bookmaker.entity.contora.Player;
import com.goodsoft.bookmaker.entity.contora.PlayersRaces;
import com.goodsoft.bookmaker.entity.contora.Race;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator.
 */
public interface PlayersRacesDAO {

    Map<Long, PlayersRaces> readAllPlayersRaces() throws SQLException, ClassNotFoundException, ParseException;

    void deletePlayerFromRace(Player player, Race race) throws SQLException, ClassNotFoundException;

    void deleteAllPlayersFromRace(Race race) throws SQLException, ClassNotFoundException;

    void addPlayerToRace(Player player, Race race) throws SQLException, ClassNotFoundException;

    List<Player> readPlayersOnRace(Race race) throws SQLException, ClassNotFoundException;

    List<PlayersRaces> readResultsOfRace(Race race) throws SQLException, ClassNotFoundException, ParseException;

    Map<Long, Race> readRacesWithPlayer(Player player) throws SQLException, ClassNotFoundException, ParseException;

}
