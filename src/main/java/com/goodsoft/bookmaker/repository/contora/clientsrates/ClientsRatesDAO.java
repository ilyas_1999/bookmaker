package com.goodsoft.bookmaker.repository.contora.clientsrates;

import com.goodsoft.bookmaker.entity.account.Client;
import com.goodsoft.bookmaker.entity.contora.ClientsRates;
import com.goodsoft.bookmaker.entity.contora.Rate;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Map;

/**
 * Created by Administrator.
 */
public interface ClientsRatesDAO {

    Map<Long, ClientsRates> readAllClientRates() throws SQLException, ClassNotFoundException, ParseException;

    void addRateToClient(Client client, Rate rate, Double amount) throws SQLException, ClassNotFoundException;
}
