package com.goodsoft.bookmaker.repository.contora.track;

import com.goodsoft.bookmaker.entity.contora.Track;

import java.sql.SQLException;
import java.util.Map;

/**
 * Created by Administrator.
 */
public interface TrackDAO {

    Map<Long, Track> readAllTracks() throws SQLException, ClassNotFoundException;

    Track findTrackById(Long id) throws SQLException, ClassNotFoundException;

    Long findIdByName(String name) throws SQLException, ClassNotFoundException;

    void addTrack(Track track) throws SQLException, ClassNotFoundException;

    void editTrack(Long id, Track track) throws SQLException, ClassNotFoundException;

    void deleteTrack(Long id) throws SQLException, ClassNotFoundException;
}
