package com.goodsoft.bookmaker.repository.contora.race;

import com.goodsoft.bookmaker.entity.contora.Race;
import com.goodsoft.bookmaker.storage.JdbcConnector;
import org.springframework.stereotype.Repository;

import java.lang.reflect.InvocationTargetException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Repository
public class RaceDaoImpl implements RaceDao {

    public Map<Long, Race> readAllRaces() throws SQLException, ClassNotFoundException, ParseException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.race ORDER BY \"date\" DESC");
        ResultSet rs = st.executeQuery();
        Map<Long, Race> races = new LinkedHashMap<>();
        Race race;
        while (rs.next()) {
            race = new Race();
            race.setDate(rs.getString(2));
            race.setTime(rs.getString(6));
            race.setName(rs.getString(5));
            races.put(rs.getLong(1), race);
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return races;
    }

    public Map<Long, Race> readCurrentRaces() throws SQLException, ClassNotFoundException, ParseException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.race WHERE \"date\" >= now()::DATE ORDER BY \"date\" DESC ");
        ResultSet rs = st.executeQuery();
        Map<Long, Race> races = new LinkedHashMap<>();
        Race race;
        while (rs.next()) {
            if (rs.getLong(4) == 1) {
                race = new Race();
                race.setDate(rs.getString(2));
                race.setTime(rs.getString(6));
                race.setName(rs.getString(5));
                races.put(rs.getLong(1), race);
            }
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return races;
    }

    public Map<Long, Race> readPastRaces() throws SQLException, ClassNotFoundException, ParseException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.race WHERE \"date\" < now()::DATE ORDER BY \"date\" DESC ");
        ResultSet rs = st.executeQuery();
        Map<Long, Race> races = new LinkedHashMap<>();
        Race race;
        while (rs.next()) {
            race = new Race();
            race.setDate(rs.getString(2));
            race.setTime(rs.getString(6));
            race.setName(rs.getString(5));
            races.put(rs.getLong(1), race);
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return races;
    }

    public List<Race> readStartedRaces() throws SQLException, ClassNotFoundException, ParseException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.race WHERE \"date\" = now()::DATE ORDER BY \"date\" DESC ");
        ResultSet rs = st.executeQuery();
        List<Race> races = new ArrayList<>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Race race;
        while (rs.next()) {
            if ((Calendar.getInstance().getTimeInMillis()
                    - simpleDateFormat.parse(rs.getString(2) + " " + rs.getString(6)).getTime()) / 1000 > 1
                    && (Calendar.getInstance().getTimeInMillis()
                    - simpleDateFormat.parse(rs.getString(2) + " " + rs.getString(6)).getTime()) / 1000 < 3600
                    && rs.getLong(4) == 3) {
                race = new Race();
                race.setDate(rs.getString(2));
                race.setTime(rs.getString(6));
                race.setName(rs.getString(5));
                races.add(race);
            }
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return races;
    }

    public Map<Long, Race> readTodayRaces() throws SQLException, ClassNotFoundException, ParseException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.race WHERE \"date\" = now()::DATE ORDER BY \"date\" DESC ");
        ResultSet rs = st.executeQuery();
        Map<Long, Race> races = new LinkedHashMap<>();
        Race race;
        while (rs.next()) {
            race = new Race();
            race.setDate(rs.getString(2));
            race.setTime(rs.getString(6));
            race.setName(rs.getString(5));
            races.put(rs.getLong(1), race);
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return races;
    }

    public Race findRaceById(Long id) throws SQLException, ClassNotFoundException, ParseException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.race WHERE id=?");
        st.setLong(1, id);
        ResultSet rs = st.executeQuery();
        Race race = new Race();
        while (rs.next()) {
            race.setDate(rs.getString(2));
            race.setTime(rs.getString(6));
            race.setName(rs.getString(5));
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return race;
    }

    public void deleteRace(Long id) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("DELETE FROM bookmaker.players_races WHERE race_id=?");
        st.setLong(1, id);
        st.executeUpdate();
        st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.rate_parameters WHERE race_id=?");
        st.setLong(1, id);
        ResultSet rs = st.executeQuery();
        List<Long> deletedRates = new ArrayList<>();
        while (rs.next()) {
            deletedRates.add(rs.getLong(1));
        }
        st = jdbcConnector.getConnection()
                .prepareStatement("DELETE FROM bookmaker.rate_parameters WHERE race_id=?");
        st.setLong(1, id);
        st.executeUpdate();

        for (Long l : deletedRates) {
            st = jdbcConnector.getConnection()
                    .prepareStatement("DELETE FROM bookmaker.clients_rates WHERE rate_id=?");
            st.setLong(1, l);
            st.executeUpdate();
        }
        st = jdbcConnector.getConnection()
                .prepareStatement("DELETE FROM bookmaker.race WHERE id=?");
        st.setLong(1, id);
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }

    public void addRace(Race race, Long track_id) throws SQLException, ClassNotFoundException, ParseException, InvocationTargetException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("INSERT INTO bookmaker.race (\"date\", \"name\", \"time\", \"track_id\", \"status_id\") VALUES (?,?,?,?,1);");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        st.setDate(1, new Date(simpleDateFormat.parse(race.getDate()).getTime()));
        st.setString(2, race.getName());
        st.setString(3, race.getTime());
        st.setLong(4, track_id);
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }

    public void editRace(Long id, Race race, Long track_id) throws SQLException, ClassNotFoundException, ParseException, InvocationTargetException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("UPDATE bookmaker.race SET \"date\"=?, \"name\"=?, \"time\"=?, \"track_id\"=? WHERE id=?");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        st.setDate(1, new Date(simpleDateFormat.parse(race.getDate()).getTime()));
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("HH:mm");
        simpleDateFormat2.setTimeZone(TimeZone.getTimeZone("UTC"));
        st.setString(2, race.getName());
        st.setString(3, race.getTime());
        st.setLong(4, track_id);
        st.setLong(5, id);
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }

    public Long findIdByName(String name) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.race WHERE \"name\"=?");
        st.setString(1, name);
        ResultSet rs = st.executeQuery();
        Long id = null;
        while (rs.next()) {
            id = rs.getLong(1);
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return id;
    }
}
