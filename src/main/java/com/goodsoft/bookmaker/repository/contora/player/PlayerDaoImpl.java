package com.goodsoft.bookmaker.repository.contora.player;

import com.goodsoft.bookmaker.entity.contora.Player;
import com.goodsoft.bookmaker.storage.JdbcConnector;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator.
 */
@Repository
public class PlayerDaoImpl implements PlayerDao {

    public List<Player> readAllPlayers() throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.player ORDER BY \"jokey\"");
        ResultSet rs = st.executeQuery();
        List<Player> players = new ArrayList<>();
        Player player;
        while (rs.next()) {
            player = new Player();
            player.setJokey(rs.getString(2));
            player.setHorse(rs.getString(3));
            players.add(player);
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return players;
    }

    public Player findPlayerById(Long id) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.player WHERE id=?");
        st.setLong(1, id);
        ResultSet rs = st.executeQuery();
        Player player = new Player();
        while (rs.next()) {
            player.setJokey(rs.getString(2));
            player.setHorse(rs.getString(3));
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return player;
    }

    public Long findIdByJokey(String jokey) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT id FROM bookmaker.Player WHERE \"jokey\"=?");
        st.setString(1, jokey);
        ResultSet rs = st.executeQuery();
        Long id = null;
        while (rs.next()) {
            id = rs.getLong(1);
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return id;
    }

    public void deletePlayer(Long id) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("DELETE FROM bookmaker.players_races WHERE player_id=?");
        st.setLong(1, id);
        st.executeUpdate();
        st = jdbcConnector.getConnection()
                .prepareStatement("DELETE FROM bookmaker.player WHERE id=?");
        st.setLong(1, id);
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }

    public void editPlayer(Long id, Player player) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("UPDATE bookmaker.player SET \"jokey\"=?,\"horse\"=? WHERE id=?");
        st.setString(1, player.getJokey());
        st.setString(2, player.getHorse());
        st.setLong(3, id);
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }

    public void addPlayer(Player player) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("INSERT INTO bookmaker.player (\"jokey\",\"horse\") VALUES (?,?)");
        st.setString(1, player.getJokey());
        st.setString(2, player.getHorse());
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }
}
