package com.goodsoft.bookmaker.repository.contora.ratetype;

import com.goodsoft.bookmaker.entity.contora.RateType;

import java.sql.SQLException;
import java.util.Map;

/**
 * Created by Administrator.
 */
public interface RateTypeDAO {

    Map<Long, RateType> readAllRateTypes() throws SQLException, ClassNotFoundException;

    RateType findRateTypeById(Long id) throws SQLException, ClassNotFoundException;

    Long findIdByName(String name) throws SQLException, ClassNotFoundException;
}
