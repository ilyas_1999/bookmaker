package com.goodsoft.bookmaker.repository.contora.clientsrates;

import com.goodsoft.bookmaker.repository.account.client.ClientDao;
import com.goodsoft.bookmaker.repository.contora.player.PlayerDao;
import com.goodsoft.bookmaker.repository.contora.race.RaceDao;
import com.goodsoft.bookmaker.repository.contora.rate.RateDao;
import com.goodsoft.bookmaker.repository.contora.ratetype.RateTypeDAO;
import com.goodsoft.bookmaker.entity.account.Client;
import com.goodsoft.bookmaker.entity.contora.ClientsRates;
import com.goodsoft.bookmaker.entity.contora.Rate;
import com.goodsoft.bookmaker.storage.JdbcConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Administrator.
 */
@Repository
public class ClientsRatesDAOImpl implements ClientsRatesDAO {

    @Autowired
    private ClientDao clientDao;
    @Autowired
    private RateDao rateDao;
    @Autowired
    private PlayerDao playerDao;
    @Autowired
    private RaceDao raceDao;
    @Autowired
    private RateTypeDAO rateTypeDAO;

    public Map<Long, ClientsRates> readAllClientRates() throws SQLException, ClassNotFoundException, ParseException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.clients_rates");
        ResultSet rs = st.executeQuery();
        Map<Long, ClientsRates> clientsRates = new LinkedHashMap<>();
        while (rs.next()) {
            clientsRates.put(Long.parseLong(rs.getString(1)), new ClientsRates(clientDao.findClientById(rs.getLong(2)), rateDao.findRateById(rs.getLong(3)), rs.getDouble(4)));
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return clientsRates;
    }

    public void addRateToClient(Client client, Rate rate, Double amount) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("INSERT INTO bookmaker.clients_rates (\"client_id\", \"rate_id\", \"amount\") VALUES (?,?,?);");
        st.setLong(1, clientDao.findIdByPassport(client.getPassport()));
        st.setLong(2, rateDao.findIdByAll(playerDao.findIdByJokey(rate.getPlayer().getJokey()),
                raceDao.findIdByName(rate.getRace().getName()), rateTypeDAO.findIdByName(rate.getRateType().getName())));
        st.setDouble(3, amount);
        st.executeUpdate();

        st = jdbcConnector.getConnection()
                .prepareStatement("UPDATE bookmaker.client SET \"balance\" = \"balance\"-? WHERE id=?");
        st.setDouble(1, amount);
        st.setLong(2, clientDao.findIdByPassport(client.getPassport()));
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }
}
