package com.goodsoft.bookmaker.repository.contora.racestatus;

import com.goodsoft.bookmaker.entity.contora.RaceStatus;
import com.goodsoft.bookmaker.storage.JdbcConnector;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Administrator.
 */
@Repository
public class RaceStatusDAOImpl implements RaceStatusDAO {

    public RaceStatus findRaceStatusByRaceId(Long id) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.race_status rs WHERE rs.id = (SELECT status_id FROM bookmaker.race r WHERE r.id=?");
        st.setLong(1, id);
        ResultSet rs = st.executeQuery();
        RaceStatus raceStatus = null;
        while (rs.next()) {
            raceStatus = new RaceStatus(rs.getString(2));
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return raceStatus;
    }
}
