package com.goodsoft.bookmaker.repository.contora.ratetype;

import com.goodsoft.bookmaker.entity.contora.RateType;
import com.goodsoft.bookmaker.storage.JdbcConnector;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator.
 */
@Repository
public class RateTypeDAOImpl implements RateTypeDAO {

    public Map<Long, RateType> readAllRateTypes() throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.rate_type");
        ResultSet rs = st.executeQuery();
        Map<Long, RateType> rateTypes = new HashMap<>();
        while (rs.next()) {
            rateTypes.put(Long.parseLong(rs.getString(1)), new RateType(rs.getString(2)));
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return rateTypes;
    }

    public RateType findRateTypeById(Long id) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.rate_type WHERE id=?");
        st.setLong(1, id);
        ResultSet rs = st.executeQuery();
        RateType rateType = null;
        while (rs.next()) {
            rateType = new RateType(rs.getString(2));
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return rateType;
    }

    public Long findIdByName(String name) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT id FROM bookmaker.rate_type WHERE \"name\"=?");
        st.setString(1, name);
        ResultSet rs = st.executeQuery();
        Long id = null;
        while (rs.next()) {
            id=rs.getLong(1);
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return id;
    }
}
