package com.goodsoft.bookmaker.repository.contora.player;

import com.goodsoft.bookmaker.entity.contora.Player;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Administrator.
 */
public interface PlayerDao {

    List<Player> readAllPlayers() throws SQLException, ClassNotFoundException;

    Player findPlayerById(Long id) throws SQLException, ClassNotFoundException;

    Long findIdByJokey(String jokey) throws SQLException, ClassNotFoundException;

    void deletePlayer(Long id) throws SQLException, ClassNotFoundException;

    void editPlayer(Long id, Player player) throws SQLException, ClassNotFoundException;

    void addPlayer(Player player) throws SQLException, ClassNotFoundException;
}
