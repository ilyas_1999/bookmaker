package com.goodsoft.bookmaker.repository.contora.rate;

import com.goodsoft.bookmaker.repository.contora.player.PlayerDao;
import com.goodsoft.bookmaker.repository.contora.race.RaceDao;
import com.goodsoft.bookmaker.repository.contora.ratetype.RateTypeDAO;
import com.goodsoft.bookmaker.entity.contora.Player;
import com.goodsoft.bookmaker.entity.contora.Race;
import com.goodsoft.bookmaker.entity.contora.Rate;
import com.goodsoft.bookmaker.entity.contora.RateType;
import com.goodsoft.bookmaker.storage.JdbcConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator.
 */
@Repository
public class RateDaoImpl implements RateDao {

    @Autowired
    private RateTypeDAO rateTypeDAO;
    @Autowired
    private RaceDao raceDao;
    @Autowired
    private PlayerDao playerDao;

    public Map<Long, Rate> readAllRates() throws SQLException, ClassNotFoundException, ParseException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.rate_parameters");
        ResultSet rs = st.executeQuery();
        Map<Long, Rate> rates = new HashMap<>();
        while (rs.next()) {
            rates.put(Long.parseLong(rs.getString(1)), new Rate(rs.getDouble(8), rs.getInt(2), rs.getDouble(3), rs.getDouble(4),
                    rateTypeDAO.findRateTypeById(rs.getLong(5)), raceDao.findRaceById(rs.getLong(6)), playerDao.findPlayerById(rs.getLong(7))));
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return rates;
    }

    public Rate findRateById(Long id) throws SQLException, ClassNotFoundException, ParseException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.rate_parameters WHERE id=?");
        st.setLong(1, id);
        ResultSet rs = st.executeQuery();
        Rate rate = null;
        while (rs.next()) {
            rate = new Rate(rs.getDouble(8), rs.getInt(2), rs.getDouble(3), rs.getDouble(4),
                    rateTypeDAO.findRateTypeById(rs.getLong(5)), raceDao.findRaceById(rs.getLong(6)),
                    playerDao.findPlayerById(rs.getLong(7)));
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return rate;
    }

    public Long findIdByAll(Long playerId, Long raceId, Long rateTypeId) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.rate_parameters rp " +
                        "WHERE rp.player_id=? AND rp.race_id =? AND rp.rate_type_id=?");
        st.setLong(1, playerId);
        st.setLong(2, raceId);
        st.setLong(3, rateTypeId);
        ResultSet rs = st.executeQuery();
        Long id = null;
        while (rs.next()) {
            id = rs.getLong(1);
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return id;
    }

    public void addRate(Rate rate) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("INSERT INTO bookmaker.rate_parameters" +
                        "(coefficient,player_id,race_id,rate_type_id,max_summ,min_summ,max_count) " +
                        "VALUES (?,?,?,?,?,?,?)");
        st.setDouble(1, rate.getCoefficient());
        st.setLong(2, playerDao.findIdByJokey(rate.getPlayer().getJokey()));
        st.setLong(3, raceDao.findIdByName(rate.getRace().getName()));
        st.setLong(4, rateTypeDAO.findIdByName(rate.getRateType().getName()));
        st.setDouble(5, rate.getMaxsumm());
        st.setDouble(6, rate.getMinsumm());
        st.setInt(7, rate.getMaxcount());
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }

    public void deleteRate(Long id) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("DELETE FROM bookmaker.rate_parameters WHERE id=?");
        st.setLong(1, id);
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }

    public void editRate(Long id, Rate rate, Race race, RateType rateType, Player player)
            throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("INSERT INTO bookmaker.rate_parameters (\"coefficient\",\"max_count\",\"min_summ\"," +
                        "\"max_summ\",\"rate_type_id\",\"race_id\",\"player_id\") VALUES (?,?,?,?,?,?,?) WHERE id=?");
        st.setDouble(1, rate.getCoefficient());
        st.setInt(2, rate.getMaxcount());
        st.setDouble(3, rate.getMinsumm());
        st.setDouble(4, rate.getMaxsumm());
        st.setLong(5, rateTypeDAO.findIdByName(rateType.getName()));
        st.setLong(6, raceDao.findIdByName(race.getName()));
        st.setLong(7, playerDao.findIdByJokey(player.getJokey()));
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }
}
