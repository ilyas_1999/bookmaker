package com.goodsoft.bookmaker.repository.contora.racestatus;

import com.goodsoft.bookmaker.entity.contora.RaceStatus;

import java.sql.SQLException;

/**
 * Created by Administrator.
 */
public interface RaceStatusDAO {

    RaceStatus findRaceStatusByRaceId(Long id) throws SQLException, ClassNotFoundException;
}
