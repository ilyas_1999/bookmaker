package com.goodsoft.bookmaker.repository.account.user;

import com.goodsoft.bookmaker.entity.account.User;
import com.goodsoft.bookmaker.storage.JdbcConnector;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator.
 */
@Repository
public class UserDAOImpl implements UserDAO {

    public UserDAOImpl() {

    }

    public List<User> readAllUsers() throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.user");
        ResultSet rs = st.executeQuery();
       List<User> users = new ArrayList<>();
        User user;
        while (rs.next()) {
            user = new User();
            user.setLogin(rs.getString(2));
            user.setPassword(rs.getString(3));
            users.add(user);
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return users;
    }

    public List<String> readAllLogins() throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT \"login\" FROM bookmaker.user");
        ResultSet rs = st.executeQuery();
        List<String> logins = new ArrayList<>();
        while (rs.next()) {
            logins.add(rs.getString(1));
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return logins;
    }

    public User findUserById(Long id) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.user WHERE id=?");
        st.setLong(1, id);
        ResultSet rs = st.executeQuery();
        User user = new User();
        while (rs.next()) {
            user .setLogin(rs.getString(2));
            user.setPassword(rs.getString(3));
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return user;
    }

    public Long findIdByLogin(String login) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT id FROM bookmaker.user WHERE \"login\"=?");
        st.setString(1, login);
        ResultSet rs = st.executeQuery();
        Long id = null;
        while (rs.next()) {
            id = rs.getLong(1);
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return id;
    }

    public void addUser(User user) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("INSERT INTO bookmaker.user(\"login\",\"password\") VALUES(?,?);");
        st.setString(1, user.getLogin());
        st.setString(2, user.getPassword());
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }

    public void editUser(Long id, User user) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("UPDATE bookmaker.user SET \"login\"=?, \"password\"=? WHERE id=?");
        st.setString(1, user.getLogin());
        st.setString(2, user.getPassword());
        st.setLong(3, id);
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }

    public void deleteUser(User user) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("DELETE FROM bookmaker.clients_rates WHERE client_id=?");
        st.setLong(1, findIdByLogin(user.getLogin()));
        st = jdbcConnector.getConnection()
                .prepareStatement("DELETE FROM bookmaker.client WHERE user_id=?");
        st.setLong(1, findIdByLogin(user.getLogin()));
        st.executeUpdate();
        st = jdbcConnector.getConnection()
                .prepareStatement("DELETE FROM bookmaker.user WHERE id=?");
        st.setLong(1, findIdByLogin(user.getLogin()));
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }
}
