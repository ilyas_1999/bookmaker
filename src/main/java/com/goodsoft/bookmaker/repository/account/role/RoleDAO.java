package com.goodsoft.bookmaker.repository.account.role;

import com.goodsoft.bookmaker.entity.account.Role;

import java.sql.SQLException;
import java.util.Map;

/**
 * Created by Administrator.
 */
public interface RoleDAO {

    Map<Long,Role> readAllRoles() throws SQLException, ClassNotFoundException;
    Role findRoleById(Long id) throws SQLException, ClassNotFoundException;
    Long findIdByName(String name) throws SQLException, ClassNotFoundException;
}
