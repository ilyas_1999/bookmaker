package com.goodsoft.bookmaker.repository.account.rolesusers;

import com.goodsoft.bookmaker.repository.account.role.RoleDAO;
import com.goodsoft.bookmaker.repository.account.user.UserDAO;
import com.goodsoft.bookmaker.entity.account.Role;
import com.goodsoft.bookmaker.entity.account.User;
import com.goodsoft.bookmaker.storage.JdbcConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Administrator.
 */
@Repository
public class RolesUsersDAOImpl implements RolesUsersDAO {

    @Autowired
    private UserDAO userDAO;
    @Autowired
    private RoleDAO roleDAO;

    public void addUserToRole(User user, Role role) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("INSERT INTO bookmaker.roles_users (role_id,user_id) VALUES (?,?);");
        st.setLong(1, roleDAO.findIdByName(role.getName()));
        st.setLong(2, userDAO.findIdByLogin(user.getLogin()));
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }

    public String findNameByUserId(Long id) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT \"name\" FROM bookmaker.role JOIN bookmaker.roles_users ru ON ru.role_id=role.id AND ru.user_id=?");
        st.setLong(1, id);
        ResultSet rs = st.executeQuery();
        String name = null;
        while (rs.next()) {
            name = rs.getString(1);
        }
        return name;
    }

    public void deleteUserFromRole(Long id) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("DELETE FROM bookmaker.roles_users WHERE user_id=?;");
        st.setLong(1, id);
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }
}
