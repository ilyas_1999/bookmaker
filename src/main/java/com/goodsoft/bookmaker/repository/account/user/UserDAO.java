package com.goodsoft.bookmaker.repository.account.user;

import com.goodsoft.bookmaker.entity.account.User;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Administrator.
 */
public interface UserDAO{

    List<User> readAllUsers() throws SQLException, ClassNotFoundException;

    List<String> readAllLogins() throws SQLException, ClassNotFoundException;

    User findUserById(Long id) throws SQLException, ClassNotFoundException;

    Long findIdByLogin(String login) throws SQLException, ClassNotFoundException;

    void addUser(User user) throws SQLException, ClassNotFoundException;

    void editUser(Long id, User user) throws SQLException, ClassNotFoundException;

    void deleteUser(User user) throws SQLException, ClassNotFoundException;
}
