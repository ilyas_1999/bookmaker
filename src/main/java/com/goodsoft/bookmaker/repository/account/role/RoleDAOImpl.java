package com.goodsoft.bookmaker.repository.account.role;

import com.goodsoft.bookmaker.entity.account.Role;
import com.goodsoft.bookmaker.storage.JdbcConnector;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator.
 */
@Repository
public class RoleDAOImpl implements RoleDAO {

    public Map<Long, Role> readAllRoles() throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.role");
        ResultSet rs = st.executeQuery();
        Map<Long, Role> roles = new HashMap<>();
        Role role;
        while (rs.next()) {
            role = new Role();
            role.setName(rs.getString(2));
            roles.put(rs.getLong(1), role);
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return roles;
    }

    public Role findRoleById(Long id) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.role WHERE id=?");
        st.setLong(1, id);
        ResultSet rs = st.executeQuery();
        Role role = new Role();
        while (rs.next()) {
            role.setName(rs.getString(2));
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return role;
    }

    public Long findIdByName(String name) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT id FROM bookmaker.role WHERE \"name\"=?");
        st.setString(1, name);
        ResultSet rs = st.executeQuery();
        Long id = null;
        while (rs.next()) {
            id=rs.getLong(1);
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return id;
    }
}
