package com.goodsoft.bookmaker.repository.account.client;

import com.goodsoft.bookmaker.entity.account.Client;
import com.goodsoft.bookmaker.entity.account.Role;
import com.goodsoft.bookmaker.entity.account.User;
import com.goodsoft.bookmaker.repository.account.rolesusers.RolesUsersDAO;
import com.goodsoft.bookmaker.repository.account.user.UserDAO;
import com.goodsoft.bookmaker.storage.JdbcConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator.
 */
@Repository
public class ClientDaoImpl implements ClientDao  {

    @Autowired
    private RolesUsersDAO rolesUsersDAO;
    @Autowired
    private UserDAO userDAO;

    public Map<Long, Client> readAllClients() throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.client");
        ResultSet rs = st.executeQuery();
        Map<Long, Client> clients = new HashMap<>();
        Client client = new Client();
        while (rs.next()) {
            client.setFirstName(rs.getString(2));
            client.setSecondName(rs.getString(3));
            client.setPassport(rs.getString(4));
            client.setBalance(rs.getDouble(6));
            clients.put(Long.parseLong(rs.getString(1)),client);
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return clients;
    }

    public Client findClientById(Long id) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT * FROM bookmaker.client WHERE id=?");
        st.setLong(1, id);
        ResultSet rs = st.executeQuery();
        Client client = new Client();
        while (rs.next()) {
            client.setFirstName(rs.getString(2));
            client.setSecondName(rs.getString(3));
            client.setPassport(rs.getString(4));
            client.setBalance(rs.getDouble(6));
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return client;
    }

    public Long findIdByPassport(String passport) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("SELECT id FROM bookmaker.client WHERE \"passport\"=?");
        st.setString(1, passport);
        ResultSet rs = st.executeQuery();
        Long id = null;
        while (rs.next()) {
            id = rs.getLong(1);
        }
        rs.close();
        st.close();
        jdbcConnector.getConnection().close();
        return id;
    }

    public void addClient(Client client, User user) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("INSERT INTO bookmaker.user(\"login\",\"password\") VALUES (?,?);");
        st.setString(1, user.getLogin());
        st.setString(2, user.getPassword());
        st.executeUpdate();
        Role role = new Role();
        role.setName("client");
        rolesUsersDAO.addUserToRole(user, role);
        st = jdbcConnector.getConnection()
                .prepareStatement("INSERT INTO bookmaker.client (\"first_name\", \"second_name\", \"passport\", \"user_id\", \"balance\") VALUES (?,?,?,?,?);");
        st.setString(1, client.getFirstName());
        st.setString(2, client.getSecondName());
        st.setString(3, client.getPassport());
        st.setLong(4, userDAO.findIdByLogin(user.getLogin()));
        st.setDouble(5, client.getBalance());
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }

    public void editClient(Long id, Client client) throws SQLException, ClassNotFoundException {
        JdbcConnector jdbcConnector = new JdbcConnector();
        PreparedStatement st = jdbcConnector.getConnection()
                .prepareStatement("UPDATE bookmaker.client SET \"first_name\"=?, \"second_name\"=?, \"passport\"=?, \"balance\"=? WHERE id=?");
        st.setString(1, client.getFirstName());
        st.setString(2, client.getSecondName());
        st.setString(3, client.getPassport());
        st.setDouble(4, client.getBalance());
        st.setLong(5, id);
        st.executeUpdate();
        st.close();
        jdbcConnector.getConnection().close();
    }
}
