package com.goodsoft.bookmaker.web;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import java.io.IOException;

/**
 * Created by Administrator.
 */
@Component
public class CharacterEncoding implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        servletRequest.setCharacterEncoding("utf-8");
          /*  if (((HttpServletRequest) servletRequest).getSession().getAttribute("user_login")==null
                    || ((HttpServletRequest) servletRequest).getSession().getAttribute("user_password")==null) {
                ((HttpServletResponse)servletResponse).sendRedirect("/login.html");
        }*/
        filterChain.doFilter(servletRequest, servletResponse);
    }

    public void destroy() {

    }
}
