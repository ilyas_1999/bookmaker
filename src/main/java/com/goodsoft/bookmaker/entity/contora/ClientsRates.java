package com.goodsoft.bookmaker.entity.contora;

import com.goodsoft.bookmaker.entity.account.Client;

import java.io.Serializable;

/**
 * Created by Administrator.
 */
public class ClientsRates implements Serializable {

    private Client client;
    private Rate rate;
    private Double amount;

    public ClientsRates(Client client, Rate rate, Double amount) {
        this.client = client;
        this.rate = rate;
        this.amount = amount;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Rate getRate() {
        return rate;
    }

    public void setRate(Rate rate) {
        this.rate = rate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
