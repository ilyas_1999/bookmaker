package com.goodsoft.bookmaker.entity.contora;

import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;

/**
 * Created by Administrator.
 */
public class Track implements Serializable {

    @NotEmpty
    private String name;
    @NotEmpty
    private String type;
    @NotEmpty
    private String city;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
