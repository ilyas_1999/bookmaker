package com.goodsoft.bookmaker.entity.contora;

import java.io.Serializable;

/**
 * Created by Administrator.
 */
public class Rate implements Serializable {

    private Double coefficient;
    private Integer maxcount;
    private Double minsumm;
    private Double maxsumm;
    private RateType rateType;
    private Race race;
    private Player player;

    public Rate(Double coefficient, Integer maxcount, Double minsumm, Double maxsumm, RateType rateType, Race race, Player player) {
        this.coefficient = coefficient;
        this.maxcount = maxcount;
        this.minsumm = minsumm;
        this.maxsumm = maxsumm;
        this.rateType = rateType;
        this.race = race;
        this.player = player;
    }

    public Double getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(Double coefficient) {
        this.coefficient = coefficient;
    }

    public Integer getMaxcount() {
        return maxcount;
    }

    public void setMaxcount(Integer maxcount) {
        this.maxcount = maxcount;
    }

    public Double getMinsumm() {
        return minsumm;
    }

    public void setMinsumm(Double minsumm) {
        this.minsumm = minsumm;
    }

    public Double getMaxsumm() {
        return maxsumm;
    }

    public void setMaxsumm(Double maxsumm) {
        this.maxsumm = maxsumm;
    }

    public RateType getRateType() {
        return rateType;
    }

    public void setRateType(RateType rateType) {
        this.rateType = rateType;
    }

    public Race getRace() {
        return race;
    }

    public void setRace(Race race) {
        this.race = race;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
