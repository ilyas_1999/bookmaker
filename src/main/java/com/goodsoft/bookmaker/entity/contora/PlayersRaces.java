package com.goodsoft.bookmaker.entity.contora;

import java.io.Serializable;

/**
 * Created by Administrator.
 */
public class PlayersRaces implements Serializable {

    private Player player;
    private Race race;
    private Integer place;

    public PlayersRaces(Player player, Race race, Integer place) {
        this.player = player;
        this.race = race;
        this.place = place;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Race getRace() {
        return race;
    }

    public void setRace(Race race) {
        this.race = race;
    }

    public Integer getPlace() {
        return place;
    }

    public void setPlace(Integer place) {
        this.place = place;
    }
}
