package com.goodsoft.bookmaker.entity.contora;

import java.io.Serializable;

/**
 * Created by Administrator.
 */
public class RateType implements Serializable {

    private String name;

    public RateType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
