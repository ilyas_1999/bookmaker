package com.goodsoft.bookmaker.entity.contora;

import java.io.Serializable;

/**
 * Created by Administrator.
 */
public class RaceStatus implements Serializable {

    private String name;

    public RaceStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
