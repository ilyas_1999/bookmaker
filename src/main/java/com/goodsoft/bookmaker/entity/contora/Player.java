package com.goodsoft.bookmaker.entity.contora;

import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;

/**
 * Created by Administrator.
 */
public class Player implements Serializable {

    @NotEmpty
    private String jokey;
    @NotEmpty
    private String horse;

    public String getJokey() {
        return jokey;
    }

    public void setJokey(String jokey) {
        this.jokey = jokey;
    }

    public String getHorse() {
        return horse;
    }

    public void setHorse(String horse) {
        this.horse = horse;
    }
}
