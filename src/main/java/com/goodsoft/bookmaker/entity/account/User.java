package com.goodsoft.bookmaker.entity.account;

import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;

/**
 * Created by Administrator.
 */
public class User implements Serializable {

    @NotEmpty
    private String login;
    @NotEmpty
    private String password;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

   /* @Override
    public String toString() {
        return "User [login=" + login + ", password=" + password + "]";
    }*/
}
