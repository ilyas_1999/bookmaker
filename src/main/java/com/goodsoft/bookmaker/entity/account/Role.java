package com.goodsoft.bookmaker.entity.account;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Administrator.
 */
public class Role implements Serializable {

    @NotNull
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
