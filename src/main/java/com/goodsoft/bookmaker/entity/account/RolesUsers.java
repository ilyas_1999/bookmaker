package com.goodsoft.bookmaker.entity.account;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Administrator.
 */
public class RolesUsers implements Serializable {

    @NotNull
    private Role role;
    @NotNull
    private User user;

    public RolesUsers(Role role, User user) {
        this.role = role;
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
