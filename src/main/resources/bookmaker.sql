﻿--drop schema bookmaker cascade
create schema bookmaker

CREATE TABLE bookmaker.track (
	"id" serial PRIMARY KEY,
	"name" TEXT NOT NULL UNIQUE,
	"type" TEXT NOT NULL,
	"city" TEXT NOT NULL
);

CREATE TABLE bookmaker.race_status (
	"id" serial PRIMARY KEY,
	"name" TEXT NOT NULL
);

CREATE TABLE bookmaker.race (
	"id" serial PRIMARY KEY,
	"name" TEXT UNIQUE,
	"date" DATE NOT NULL,
	"track_id" bigserial NOT NULL,
	"status_id" INT NOT NULL,
	CONSTRAINT "race_fk0" FOREIGN KEY ("track_id") REFERENCES bookmaker.track("id"),
	CONSTRAINT "race_fk1" FOREIGN KEY ("status_id") REFERENCES bookmaker.race_status("id")
);

CREATE TABLE bookmaker.player (
	"id" serial PRIMARY KEY,
	"jokey" TEXT UNIQUE NOT NULL,
	"horse" TEXT NOT NULL
);

CREATE TABLE bookmaker.role(
	"id" serial PRIMARY KEY,
	"name" TEXT NOT NULL UNIQUE
);


CREATE TABLE bookmaker.user (
	"id" serial PRIMARY KEY,
	"login" TEXT NOT NULL UNIQUE,
	"password" TEXT NOT NULL
);

CREATE TABLE bookmaker.roles_users (
	"role_id" bigserial NOT NULL,
	"user_id" bigserial NOT NULL,
	CONSTRAINT roles_users_pk PRIMARY KEY ("role_id","user_id"),
	CONSTRAINT "roles_users_fk0" FOREIGN KEY ("role_id") REFERENCES bookmaker.role("id"),
	CONSTRAINT "roles_users_fk1" FOREIGN KEY ("user_id") REFERENCES bookmaker.user("id")
	);

CREATE TABLE bookmaker.client (
	"id" serial PRIMARY KEY,
	"first_name" TEXT,
	"second_name" TEXT NOT NULL,
	"passport" TEXT NOT NULL UNIQUE,
	"user_id" bigserial NOT NULL UNIQUE,
	"balance" BIGINT DEFAULT 0,
	CONSTRAINT "client_fk0" FOREIGN KEY ("user_id") REFERENCES bookmaker.user("id")
);

CREATE TABLE bookmaker.players_races (
	"player_id" bigserial NOT NULL,
	"race_id" bigserial NOT NULL,
	CONSTRAINT players_races_pk PRIMARY KEY ("player_id","race_id"),
	CONSTRAINT "players_races_fk0" FOREIGN KEY ("player_id") REFERENCES bookmaker.player("id"),
	CONSTRAINT "players_races_fk1" FOREIGN KEY ("race_id") REFERENCES bookmaker.race("id")
);


CREATE TABLE bookmaker.rate_type (
	"id" serial PRIMARY KEY,
	"name" TEXT NOT NULL UNIQUE
);

CREATE TABLE bookmaker.rate_parameters (
	"id" serial PRIMARY KEY,
	"max_count" BIGINT NOT NULL,
	"min_summ" INT NOT NULL,
	"max_summ" BIGINT NOT NULL,
	"rate_type_id" INT NOT NULL,
	"race_id" INT NOT NULL,
	"player_id" BIGINT NOT NULL,
	"coefficient" FLOAT NOT NULL,
	CONSTRAINT "rate_parameters_fk0" FOREIGN KEY ("rate_type_id") REFERENCES bookmaker.rate_type("id"),
	CONSTRAINT "rate_parameters_fk1" FOREIGN KEY ("race_id") REFERENCES bookmaker.race("id"),
	CONSTRAINT "rate_parameters_fk2" FOREIGN KEY ("player_id") REFERENCES bookmaker.player("id")
);
	
CREATE TABLE bookmaker.clients_rates (
	"client_id" bigserial NOT NULL,
	"rate_id" bigserial NOT NULL,
	"Value" bigserial NOT NULL,
	CONSTRAINT client_rate_pk PRIMARY KEY ("client_id","rate_id"),
	CONSTRAINT "client_rate_fk0" FOREIGN KEY ("client_id") REFERENCES bookmaker.client("id"),
	CONSTRAINT "client_rate_fk1" FOREIGN KEY ("rate_id") REFERENCES bookmaker.rate_parameters("id")
);

insert into bookmaker.role ("name") values ('admin'),('client'),('bookmaker');
select * from bookmaker.role

insert into bookmaker.race_status ("name") values ('open'),('closed');
select * from bookmaker.race_status

insert into bookmaker.track ("name","type","city") values ('a','turf','London'),('b','dirt','Canada'),('c','all','Berlin');
select * from bookmaker.track 

insert into bookmaker.race ("date","name","track_id","status_id") values ('15.03.2017 12:00','Бешеный конь',1, ('16.03.2017 12:30','batl',2,1), ('14.03.2017 13:00','petu4',3,1),('13.02.2017 12:00','puf',1,1);
SELECT * FROM bookmaker.race ORDER BY "date"
SELECT 	* FROM bookmaker.race WHERE "date" < now()

insert into bookmaker.player("jokey","horse") values('a','a'),('b','b'),('c','c');
select * from bookmaker.player

insert into bookmaker.players_races("player_id","race_id") values (1,7),(2,8),(3,9);
select* from bookmaker.players_races
select* from bookmaker.players_races where "player_id"=2

insert into bookmaker.rate_type("name") values ('win'),('first_free'),('reward');
select * from bookmaker.rate_type

insert into bookmaker.rate_parameters("coefficient","max_count","min_summ","max_summ","rate_type_id","race_id","player_id")
values (1.3, 10, 1000,10000,1,7,2),
	(1.1,20,2000,20000,2,7,2)
	select * from bookmaker.rate_parameters rp where rp.coefficient=1.3 and rp.player_id=2 and rp.race_id = 7 and rp.rate_type_id=1 and rp.max_summ=10000 and rp.min_summ=1000 and rp.max_count=10

SELECT * FROM bookmaker.players_races WHERE race_id=9

insert into bookmaker.user ("login","password") values ('bookmaker','123'), ('admin','admin'), ('user','user');

insert into bookmaker.roles_users("user_id","role_id") values (1,3), (2,1), (3,2);

INSERT INTO bookmaker.player ("jokey","horse") VALUES ('bb','bb') WHERE id=4